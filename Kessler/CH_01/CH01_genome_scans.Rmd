---
title: "Genome scans"
author: C. Kessler
date: "`r format(Sys.time(), '%d %B %Y')`"
output:
  html_document:
    theme: cosmo
    highlight: kate
    toc: true
    toc_depth: 4
    toc_float: true
    code_folding: hide
editor_options: 
  chunk_output_type: console
---

# Genome Scans

Based on https://github.com/simonhmartin/genomics_general

Method using hard called genotypes, needs all variants in .geno file to properly calulate dxy and pi

## Set up

```{bash echo=T, eval=F}
########## ########## ########## ########## ########## ########## ########## 
# set up directory
mkdir martin
cd martin

root=/home/ckessler/projects/rrg-shaferab/ckessler
GSdir=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/genome_scans
datadir=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/data

########## ########## ########## ########## ########## ########## ########## 
# get scripts
wget https://raw.githubusercontent.com/simonhmartin/genomics_general/master/VCF_processing/parseVCF.py
wget https://raw.githubusercontent.com/simonhmartin/genomics_general/master/popgenWindows.py
wget https://raw.githubusercontent.com/simonhmartin/genomics_general/master/genomics.py

########## ########## ########## ########## ########## ########## ########## 
## Set up popFile 
# column1 = samplesIDs as in .geno file, column 2 = popID
# check input file, ID are in the same order as the bam list
gunzip -c $datadir/ch01_deer_invariants.geno.gz | head -n 5 | column -t # no header, will need to create it in script

# check that the order of the bam_list is the same as sample info
bam=$(cat $datadir/bam_list | awk -F/ '$0=$NF' | sed 's/_S[0-9]*_L00[0-9].*//' | sed 's/_merged.*//')
info=$(cat $datadir/CH01_info.txt | awk '{print $1}')

diff -s <(echo $bam ) <(echo $info) # all good

# population = group = 5th column of the info file
head $datadir/CH01_info.txt

## extract 5th column from info and put it in pops
awk -F '\t' '{print $1 "\t" $5}' $datadir/CH01_info.txt > pops.txt
# remove empty lines
sed -i '/^[[:space:]]*$/d' pops.txt

## Actually we do only on species comparisons since no Sign of hybridisation
# remove pop name to keep only the species
cat pops.txt | sed 's/D_[a,s].*$/D/' > pops_species.txt 

########## ########## ########## ########## ########## ########## ########## 
# reheader .geno file 
gunzip -c $datadir/ch01_deer_invariants.geno.gz | head -n 5 | column -t # no header

# header shoul be #CHROM POS samplesIDs all tab separated
header1="#CHROM POS"
header2=$(cat pops.txt | cut -f 1 | paste -s)
echo -e $header1"\t"$header2 | sed 's/ \+/\t/g' | gzip > header.gz # gzip for the next step

sbatch -o reheader.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=reheader --wrap="zcat header.gz $datadir/ch01_deer_invariants.geno.gz | gzip > $datadir/ch01_deer_invariants_named.geno.gz"

rm header.gz 

```

## Analysis

```{bash echo=T, eval=F}
########## ########## ########## ########## ########## ########## ########## 
# luanch analysis
input=ch01_deer_invariants_named.geno.gz
output=ch01_deer_invariants_GenomeScans

sbatch -o genome_scans_species-%A.out genome_scans_species.sh ${input} ${output}

# get on local
rsync -rv ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/genome_scans/martin/*.csv Desktop/Dropbox/PhD/CH_01/Results/Genome\ scans/

```



```{r message=FALSE, warning=FALSE, results='hide', fig.show='asis'}
########## ########## ########## ########## ########## ########## ########## 
# import data
GSf <- list.files(path = "../Results/Genome scans", 
           pattern = "ch01_deer_invariants_GenomeScans_", full.names = TRUE)

ch01_dfs <- list()
for (f in GSf) {
  # extract name
  df_name <- gsub("^.*GenomeScans_", "ch01_", f)
  df_name <- gsub(".csv", "", df_name)
  ch01_dfs[[df_name]] <- read.csv(f)

}

########## ########## ########## ########## ########## ########## ########## 
# format dfs
library(reshape2)
# get maximum size for each scaffold
scaffold_size <- aggregate(end ~ scaffold, ch01_dfs[[1]], max) 
# extract scaffolds bigger than 50kbp for upcoming analyses
scaffolds_medium <- scaffold_size[scaffold_size$end > 50000, "scaffold"]
# order size
scaffolds_ordered <- scaffold_size[order(scaffold_size$end, decreasing = TRUE), ]
# loop for formatting --> window nm and colours
for (n in 1:length(ch01_dfs)) {
  # keep scaffolds bigger than 50kb
  ch01_dfs[[n]] <- ch01_dfs[[n]][ch01_dfs[[n]]$scaffold %in% scaffolds_medium, ]
  # add windows number for plots x axes
  ch01_dfs[[n]]$win_nb <- 1:nrow(ch01_dfs[[n]])
  # set alternate colour
  # get the unique scaffold name in a df
  scaffolds <- as.data.frame(unique(ch01_dfs[[n]]$scaffold))
  colnames(scaffolds) <- "Scaffolds"
  # temp with colour number, length of scaffolds + 1 
  # -> scaffolds is odd (717), will remove last entry
  tmp <- rep(x = 1:2, times = (nrow(scaffolds)+1)/2)
  # remove the last entry if needed
  tmp <- tmp[1:nrow(scaffolds)]
  # assign tmp as colour number
  scaffolds$colour <- tmp
  # merge the two
  ch01_dfs[[n]] <- merge(ch01_dfs[[n]], scaffolds, by.x = "scaffold", 
                         by.y = "Scaffolds")
  ch01_dfs[[n]]$colour <- as.character(ch01_dfs[[n]]$colour)
}

# get all the dfs back in the environment
list2env(ch01_dfs, envir = .GlobalEnv)


```


### Fst & Dxy

```{r message=FALSE, warning=FALSE, results='hide', fig.show='asis'}
library(ggplot2)
library(ggpubr)
# negative Fst values should be set to 0
ch01_popPairDist[ch01_popPairDist < 0] <- 0 

# format data
comp <- "dxy_WTD_MD,Fst_WTD_MD"
# fst & dxy colnames
dxy <- sapply(strsplit(comp, ","), getElement, 1)
fst <- sapply(strsplit(comp, ","), getElement, 2)
# comparison names clean
comparison <- gsub(pattern = "dxy_", replacement = "", x = dxy)
comparison <- gsub(pattern = "_", replacement = " & ", x = comparison)
########## ########## ########## ########## ########## ########## ########## 
# point and barplot fst & dxy
# plot dxy
p_dxy <- ggplot(ch01_popPairDist, aes_string(x = "win_nb", y = dxy, 
                                             colour = "colour")) +
  geom_point() +
  theme_minimal() +
  theme(axis.title.x = element_blank(), axis.text.x = element_blank()) +
  guides(colour = "none") +
  labs(y = "dxy") +
  scale_colour_manual(values = c("#f18e8e", "#710f0e")) +
  ylim(0, 0.8)
# plot fst
p_fst <- ggplot(ch01_popPairDist, aes_string(x = "win_nb", y = fst, 
                                             colour = "colour")) +
  geom_point() +
  theme_minimal() +
  theme(axis.title.x = element_blank(), axis.text.x = element_blank()) +
  guides(colour = "none") +
  labs(y = "Fst") +
  scale_colour_manual(values = c("#86f8ce", "#07794e")) +
  ylim(0, 0.8)
# plot fst histogram
p_fst2 <- ggplot(ch01_popPairDist, aes_string(x = fst)) + 
  geom_histogram(bins = 150, fill = "#055738") +
  theme_minimal() +
  labs(x = "Fst", y = "Frequency")
# dxy histogram
p_dxy2 <- ggplot(ch01_popPairDist, aes_string(x = dxy)) + 
  geom_histogram(bins = 150, fill = "#710f0e") +
  theme_minimal() +
  labs(x = "dxy", y = "Frequency")

plot <- ggarrange(p_dxy, p_dxy2, p_fst, p_fst2, ncol = 2, nrow = 2) # all 4 plots in one
p5 <- annotate_figure(plot, top = text_grob(paste0("\nComparison between ",
                                                   comparison)))# title
print(p5) 
ggsave("../Results/Plots/ch01_GS_div_WTD_MD.pdf")

```


### Individual heterozygosity

```{r message=FALSE, warning=FALSE, results='hide', fig.show='asis'}
heterozyg <- melt(data = ch01_indHet, measure.vars = c(6:33),
                  id.vars = "scaffold")
# get samples info
ch01_info <- read.table("../Data/CH01_info.txt", 
                        sep = "\t", header = TRUE)

# keep only sample name / merge with info
heterozyg$variable <- gsub("het_", "", heterozyg$variable)
heterozyg <- merge(heterozyg, ch01_info, by.x = "variable", 
                   by.y = "Genome_ID", all = TRUE )

ggplot(heterozyg, aes(x = variable, y = value, colour = Group)) + 
  geom_boxplot() +
  facet_grid(~Group, scales = "free_x", space = "free_x") +
  theme_minimal() +
  theme(axis.title.x = element_blank(), 
        axis.text.x = element_text(angle = 45, hjust = 1)) +
  guides(colour = "none") +
  labs(y = "Heterozygosity", title = "\n")

ggsave("../Results/Plots/ch01_GS_heterozyg.pdf")


```


### Other stats

```{r message=FALSE, warning=FALSE, results='hide', fig.show='asis'}
groups <- data.frame("WTD" = grep(pattern = "_WTD", 
                                            x = colnames(ch01_popFreq),
                                            value = TRUE),
                     "MD" = grep(pattern = "_MD", 
                                           x = colnames(ch01_popFreq), 
                                           value = TRUE))
for (g in colnames(groups)) {
  # l <- ch01_popFreq[, c(groups[1, g], "win_nb", "colour")] # lenght
  # S <- ch01_popFreq[, c(groups[2, g], "win_nb", "colour")] # segregating sites
  pi <- ch01_popDist[, c(paste0("pi_", g), "win_nb", "colour")] # row 3 = pi
  thetaW <- ch01_popFreq[, c(groups[4, g], "win_nb", "colour")]
  thetaW[, 1] <- thetaW[, 1]/50000
  tajD <- ch01_popFreq[, c(groups[5, g], "win_nb", "colour")]
########## ########## ########## ########## ########## ########## ########## 
# plots  
  p_TP <- ggplot(pi, aes_string(x = "win_nb", y = paste0("pi_", g), 
                                     colour = "colour")) +
    geom_point() +
    theme_minimal() +
    theme(axis.title.x = element_blank(), axis.text.x = element_blank()) +
    guides(colour = "none") +
    labs(y = expression(pi)) +
    scale_colour_manual(values = c("#8dc4f2", "#0d4472")) 
  
  p_TW <- ggplot(thetaW, aes_string(x = "win_nb", y = groups[4, g], 
                                    colour = "colour")) +
    geom_point() +
    theme_minimal() +
    theme(axis.title.x = element_blank(), axis.text.x = element_blank()) +
    guides(colour = "none") +
    labs(y = expression(theta[W])) +
    scale_colour_manual(values = c("#ffeb80", "#eac500")) 
  
  
  p_TD <- ggplot(tajD, aes_string(x = "win_nb", y = groups[5, g], 
                                  colour = "colour")) +
    geom_point() +
    theme_minimal() +
    theme(axis.title.x = element_blank(), axis.text.x = element_blank()) +
    guides(colour = "none") +
    labs(y = "Tajima's D") +
    scale_colour_manual(values = c("#d5a4da", "#55255b")) 
  
  # plot with ggarange
  plot <- ggarrange(p_TP, p_TW, p_TD, ncol = 1)
  print(annotate_figure(plot, top = text_grob(paste("\n", g))))
  ggsave(paste0("../Results/Plots/ch01_GS_popfreq_", g, ".pdf"), plot = plot)

  Sys.sleep(2)
}

```

### plot all stats

```{r message=FALSE, warning=FALSE, results='hide', fig.show='asis'}
# different visualisations
dist_species <- melt(data = ch01_popPairDist, measure.vars = c(6:7), 
                     id.vars = "scaffold")

ggplot(dist_species, aes(x = variable, y = value, colour = variable)) + 
  geom_violin(trim = FALSE) +
  geom_boxplot(width = 0.05) +
  theme_minimal() +
  theme(axis.title = element_blank()) +
  guides(colour = "none") +
  scale_colour_manual(values = c("#f18e8e", "#86f8ce"))

ggsave("../Results/Plots/ch01_GS_dxyFst_spdistrib.pdf")


# pi
# format
colnames(ch01_popFreq)
pi <- ch01_popDist[, 1:7]
colnames(pi)
pi <- melt(data = pi, measure.vars = c(6:7), id.vars = "scaffold")
# keep only pop name 
pi$variable <- gsub("pi_", "", pi$variable)

ggplot(pi, aes(x = variable, y = value, colour = variable)) + 
  geom_violin(trim = FALSE) +
  geom_boxplot(width = 0.05) +
  theme_minimal() +
  theme(axis.title.x = element_blank(), 
        axis.text.x = element_text(angle = 45, hjust = 1)) +
  guides(colour = "none") +
  labs(y = "pi")

ggsave("../Results/Plots/ch01_GS_pi_distrib.pdf")


# Taj's D
# format
TajD <- ch01_popFreq[, c(1:5, grep("TajD_", colnames(ch01_popFreq)))]
TajD <- melt(data = TajD, measure.vars = c(6:7), id.vars = "scaffold")

# keep only pop name 
TajD$variable <- gsub("TajD_", "", TajD$variable)

ggplot(TajD, aes(x = variable, y = value, colour = variable)) + 
  geom_violin(trim = FALSE) +
  geom_boxplot(width = 0.05) +
  theme_minimal() +
  theme(axis.title.x = element_blank(), 
        axis.text.x = element_text(angle = 45, hjust = 1)) +
  guides(colour = "none") +
  labs(y = "TajD")

ggsave("../Results/Plots/ch01_GS_TajD_distrib.pdf")

# means, mediands, t.tests
mean(ch01_popPairDist$dxy_WTD_MD, na.rm = TRUE)
median(ch01_popPairDist$dxy_WTD_MD, na.rm = TRUE)

mean(ch01_popPairDist$Fst_WTD_MD, na.rm = TRUE)
median(ch01_popPairDist$Fst_WTD_MD, na.rm = TRUE)


mean(ch01_popFreq$TajD_WTD, na.rm = TRUE)
median(ch01_popFreq$TajD_WTD, na.rm = TRUE)


mean(ch01_popFreq$TajD_MD, na.rm = TRUE)
median(ch01_popFreq$TajD_MD, na.rm = TRUE)
t.test(ch01_popFreq$TajD_WTD, ch01_popFreq$TajD_MD)


mean(ch01_popDist$pi_WTD, na.rm = TRUE)
mean(ch01_popDist$pi_MD, na.rm = TRUE)
# check t.test assumptions
# A1: Independance OK
# A2: Normality
qqnorm(ch01_popDist$pi_WTD) # Nope
qqnorm(ch01_popDist$pi_MD) # Nope
# Not normal --> Mann-Whitney U Test
# check assumptions
# A1: Ordinal or Continuous OK
# A2: Independence OK
# A3: shapes of the distributions are roughly the same OK
wilcox.test(ch01_popDist$pi_WTD, ch01_popDist$pi_MD, conf.int = TRUE)



```

### Outlier detection

#### Across all scaffolds
```{r message=FALSE, warning=FALSE, results='asis', fig.height=10}
library(plyr)
library(rowr)
library(kableExtra)
library(wesanderson)

########## ########## ########## ########## ########## ########## ########## 
scenarios <- c("Divergence with GF", "Allopatric selection", 
               "Background selection", "Balancing selection")
# prepare colours
cols <- as.vector(wes_palette("GrandBudapest1", n = 4))
names(cols) <- scenarios
########## ########## ########## ########## ########## ########## ########## 
# prepare data 
comparison <- gsub(pattern = "dxy_", replacement = "", x = dxy) # re-name 
comparison2 <- gsub(pattern = "_", replacement = " VS ", x = comparison) # clean
# prepare pi dataset
pops <- strsplit(comparison2, split = " VS ")
pop1 <- paste0("pi_", pops[[1]][1])
pop2 <- paste0("pi_", pops[[1]][2])
# extract pi for given population
subset_pi <- ch01_popDist[, 1:7]
# prepare dxy & fst df
subset_ch01_popPairDist <- ch01_popPairDist[,
                                            c(colnames(ch01_popPairDist)[1:5],
                                              fst, dxy)]
subset_metrics <- merge(subset_pi, subset_ch01_popPairDist, all = TRUE)
########## ########## ########## ########## ########## ########## ########## 
# set thresholds / prepare scenaios
thresholds_upper <- 0.95
thresholds_lower <- 0.05
  thresholds_middle <- c(0.45, 0.55)
thresholds <- c()
# compute upper and lower thresholds for each metric
for (metric in colnames(subset_metrics)[6:9]) {
  name_upper <- paste0("upper_", metric)
  thresholds[name_upper] <- quantile(subset_metrics[, metric], na.rm = TRUE,
                                     probs = thresholds_upper)
  name_lower <- paste0("lower_", metric)
  thresholds[name_lower] <- quantile(subset_metrics[, metric], na.rm = TRUE,
                                     probs = thresholds_lower)
  name_middle_u <- paste0("middle_u_", metric)
  name_middle_l <- paste0("middle_l_", metric)
  thresholds[c(name_middle_l, 
               name_middle_u)] <- quantile(subset_metrics[, metric],
                                           na.rm = TRUE, 
                                           probs = thresholds_middle)
  
}
# assign loci to scenarios in fig.1 https://www.biorxiv.org/content/10.1101/2021.08.26.457771v2.full.pdf
subset_metrics$scenario <- NA
# divergence with gene flow 
subset_metrics$scenario[subset_metrics[, fst] > thresholds[9] & # high fst
                          subset_metrics[, dxy] > thresholds[13] & # high dxy
                          subset_metrics[, pop1] < thresholds[2] & # low pi
                          subset_metrics[, pop2] < thresholds[6]] <- "Divergence with GF"
# allopatric selection
subset_metrics$scenario[subset_metrics[, fst] > thresholds[9] & # high fst
                          subset_metrics[, dxy] > thresholds[15] & # dxy between
                          subset_metrics[, dxy] < thresholds[16] & # both --> mean
                          subset_metrics[, pop1] < thresholds[2] & # low pi
                          subset_metrics[, pop2] < thresholds[6]] <- "Allopatric selection"
# background selection
subset_metrics$scenario[subset_metrics[, fst] > thresholds[9] & # high fst
                          subset_metrics[, dxy] < thresholds[14] & # low dxy
                          subset_metrics[, pop1] < thresholds[2] &# low pi
                          subset_metrics[, pop2] < thresholds[6]] <- "Background selection"
# balancing selection 
subset_metrics$scenario[subset_metrics[, fst] < thresholds[9] & # high fst
                          subset_metrics[, dxy] > thresholds[14] & # low dxy
                          subset_metrics[, pop1] > thresholds[1] &# high pi
                          subset_metrics[, pop2] > thresholds[5]] <- "Balancing selection"
########## ########## ########## ########## ########## ########## ########## 
# format
#order start correctly
subset_metrics <- arrange(subset_metrics, scaffold, start)
# add window number
subset_metrics$win_nb <- 1:nrow(subset_metrics)
# subset with outliers
subset_metrics_out <- subset_metrics[!is.na(subset_metrics$scenario), ]
# get colours
subset_metrics_out$colour <- subset_metrics_out$scenario
subset_metrics_out$colour <- revalue(subset_metrics_out$colour, cols)

########## ########## ########## ########## ########## ########## ########## 
#plot
p_dxy <- ggplot() +
  geom_point(subset_metrics, mapping = aes_string(x = "win_nb", y = dxy), 
             colour = "lightgrey", size = 0.5, alpha = 0.3) +
  geom_point(subset_metrics_out, mapping = aes_string(x = "win_nb", y = dxy,
                                                      colour = "colour"),
             size = 0.7) +
  theme_minimal() +
  theme(axis.title.x = element_blank(), axis.text.x = element_blank(), 
        legend.position = "bottom") +
  labs(y = "dxy") +
  geom_hline(yintercept = mean(subset_metrics[, dxy], na.rm = TRUE),
             colour = "black") +
  geom_hline(yintercept = median(subset_metrics[, dxy], 
                                 na.rm = TRUE),
             colour = "black", linetype = "dashed") + 
  scale_color_identity(breaks = cols,
                       labels = names(cols),
                       guide = "legend", name = "Scenarios:") +
  guides(colour = guide_legend(override.aes = list(size = 2)))
# plot fst
p_fst <- ggplot() +
  geom_point(subset_metrics, mapping = aes_string(x = "win_nb", y = fst), 
             colour = "light grey", size = 0.5, alpha = 0.3) +
  geom_point(subset_metrics_out, mapping = aes_string(x = "win_nb", y = fst,
                                                      colour = "colour"),
             size = 0.7) +
  theme_minimal() +
  theme(axis.text.x = element_blank(), axis.title.x = element_blank(), 
        legend.position = "bottom") +
  labs(y = "Fst") +
  geom_hline(yintercept = mean(subset_metrics[, fst], na.rm = TRUE),
             show.legend = TRUE) +
  geom_hline(yintercept = median(subset_metrics[, fst], 
                                 na.rm = TRUE),
             linetype = "dashed", show.legend = TRUE) + 
  scale_color_identity(breaks = cols,
                       labels = names(cols),
                       guide = "legend", name = "Scenarios:") +
  guides(colour = guide_legend(override.aes = list(size = 2)))
  p_pi1 <- ggplot() +
    geom_point(subset_metrics, mapping = aes_string(x = "win_nb", y = pop1), 
               colour = "light grey", size = 0.5, alpha = 0.3) +
    geom_point(subset_metrics_out, mapping = aes_string(x = "win_nb", y = pop1,
                                                        colour = "colour"),
               size = 0.7) +
    theme_minimal() +
    theme(axis.text.x = element_blank(), axis.title.x = element_blank(), 
          legend.position = "bottom") +
    labs(y = expression(pi), subtitle = gsub("pi_", "", pop2)) +
    #ylim(0, 0.8) +
    geom_hline(yintercept = mean(subset_metrics[, pop1], na.rm = TRUE),
               show.legend = TRUE) +
    geom_hline(yintercept = median(subset_metrics[, pop1], 
                                   na.rm = TRUE),
               linetype = "dashed", show.legend = TRUE) + 
     scale_color_identity(breaks = cols,
                          labels = names(cols),
                          guide = "legend", name = "Scenarios:") +
    guides(colour = guide_legend(override.aes = list(size = 2)))
  # PLOT POP2 
  p_pi2 <- ggplot() +
    geom_point(subset_metrics, mapping = aes_string(x = "win_nb", y = pop2), 
               colour = "light grey", size = 0.5, alpha = 0.3) +
    geom_point(subset_metrics_out, mapping = aes_string(x = "win_nb", y = pop2,
                                                        colour = "colour"),
               size = 0.7) +
    theme_minimal() +
    theme(axis.text.x = element_blank(), axis.title.x = element_blank(), 
          legend.position = "bottom") +
    labs(y = expression(pi), subtitle = gsub("pi_", "", pop2)) +
    #ylim(0, 0.8) +
    geom_hline(yintercept = mean(subset_metrics[, pop2], na.rm = TRUE),
               show.legend = TRUE) +
    geom_hline(yintercept = median(subset_metrics[, pop2], 
                                   na.rm = TRUE),
               linetype = "dashed", show.legend = TRUE) + 
     scale_color_identity(breaks = cols,
                          labels = names(cols),
                          guide = "legend", name = "Scenarios:") +
    guides(colour = guide_legend(override.aes = list(size = 2))) 

plot <- ggarrange(p_dxy, p_fst, p_pi1, p_pi2, ncol = 1, nrow = 4, 
                  common.legend = TRUE,
                  legend = "bottom")
p5 <- annotate_figure(plot, 
                      top = paste0("\nPotential outlier loci\nComparison: ",
                                   comparison2)) # title
print(p5)
ggsave("../Results/Plots/ch01_GS_allOutl_WTD_MD.pdf", plot = p5)

```


### Scenarios

```{r}
########## ########## ########## ########## ########## ########## ########## 
# create scenario table
scenario_table <- data.frame("Scenarios" = scenarios, 
                             "Fst" = c(rep(paste(">", thresholds_upper),3),
                                       paste("<", thresholds_lower)),
                             "ThetaPi" = c(rep(paste("<", thresholds_lower), 3),
                                           paste(">", thresholds_upper)),
                             "Dxy" = c(paste(">", thresholds_upper),
                                       paste("Between", thresholds_middle[1],
                                             "&", thresholds_middle[2]), 
                                       paste("<", thresholds_lower),
                                       paste(">", thresholds_upper)),
                             "Panel" = 1:4)

kable(scenario_table, format = "html", 
      col.names = c("Scenario from Shang et al. (2021)", 
                    "$F_{st}$ (percentile)", 
                    "$\\theta_{\\pi}$ (percentile)", "$D_{xy}$ (percentile)", 
                    "Panel in Shang et al. (2021)")) %>%
  kable_styling()

```

### Background selection & Balancing selection


```{r message=FALSE, warning=FALSE}
# all windows with outliers
outlier_windows <- subset_metrics[!is.na(subset_metrics$scenario), ]
# plot of outliers count
ggplot(outlier_windows, aes(x = scaffold)) + geom_bar() + 
  facet_wrap(~scenario) +
  theme(axis.text.x = element_blank()) +
  labs(title = "Count of outlier windows per scaffold", x = "Scaffold", 
       y = "Outlier windows")

ggsave("../Results/Plots/ch01_GS_allOutl_perScaff.pdf")

```

#### Zoom on scaffolds

There are `r length(unique(outlier_windows$scaffold))` scaffolds with patterns of background selection or balancing selection outlier windows

```{r message=FALSE, warning=FALSE, fig.height=16, fig.width=10}
interest_scaff <- c()
pop1 <- paste0("pi_", pops[[1]][1])
pop2 <- paste0("pi_", pops[[1]][2])
for (s in unique(outlier_windows$scaffold)) {
  plots <- list()
  # keep only data for wanted scaffold + format outlier as Y/NA
  outlier_zoom <- outlier_windows[outlier_windows$scaffold == s, ]
  
  # does this scaffold have over 10 outlier windows?
  scaff_length <- nrow(outlier_zoom)
  scaff_outl <- sum(table(outlier_zoom$scenario))
  scaff_ratio <- scaff_outl/scaff_length # percentage of outliers
  
  if (scaff_outl > 10) {
    for (sce in unique(outlier_windows$scenario)) {
      outlier_sce <- outlier_zoom
      outlier_sce$scenario[outlier_sce$scenario == sce] <- "Y"
      outlier_sce$scenario[outlier_sce$scenario != "Y"] <- "N"
      outlier_sce$scenario[is.na(outlier_sce$scenario)] <- "N"
      
      # plot Fst & Dxy
      p1 <- ggplot(outlier_sce, aes(x = start)) +
        # dxy part
        geom_point(mapping = aes_string(y = dxy, alpha = "scenario", 
                                        size = "scenario"), colour = "#f18e8e") +
        geom_smooth(mapping = aes_string(y = dxy), 
                    colour = "#f18e8e", se = FALSE) + 
        #fst part
        geom_point(mapping = aes_string(y = fst, alpha = "scenario", 
                                        size = "scenario"), colour = "#07794e") +
        geom_smooth(mapping = aes_string(y = fst), 
                    colour = "#07794e", se = FALSE) + 
        # formatting      
        scale_alpha_manual(values = c(0.2, 1)) +
        scale_size_manual(values = c(0.7, 1.5)) +
        scale_y_continuous(name = "Dxy",
                           sec.axis = sec_axis(~ .*1, name = "Fst")) + 
        theme_minimal() +
        theme(axis.text.x = element_blank(), 
              axis.title.y = element_text(color = "#f18e8e"),
              axis.title.y.right = element_text(color = "#07794e")) +
        labs(x = "Window start") +
        guides(alpha = "none", size = "none")
      
      # plot pi
      p2 <- ggplot(outlier_sce, aes(x = start)) +
        # dxy part
        geom_point(mapping = aes_string(y = pop1, alpha = "scenario", 
                                        size = "scenario"), colour = "#6495ed") +
        geom_smooth(mapping = aes_string(y = pop1), 
                    colour = "#6495ed", se = FALSE) + 
        #fst part
        geom_point(mapping = aes_string(y = pop2, alpha = "scenario", 
                                        size = "scenario"), colour = "#0d4472") +
        geom_smooth(mapping = aes_string(y = pop2), 
                    colour = "#0d4472", se = FALSE) + 
        # formatting
        scale_size_manual(values = c(0.7, 1.5)) +
        scale_y_continuous(name = pops[[1]][1],
                           sec.axis = sec_axis(~ .*1, name = pops[[1]][2])) + 
        theme_minimal() +
        theme(axis.text.x = element_blank(), 
              axis.title.y = element_text(color = "#6495ed"),
              axis.title.y.right = element_text(color = "#0d4472")) +
        labs(x = "Window start") +
        scale_alpha_manual(values = c(0.2, 1)) +
        guides(alpha = "none", size = "none")
      # arragne both plots
      plot <- ggarrange(p1, p2, ncol = 1, nrow = 2)
      scaff_outl <- length(which(outlier_sce$scenario == "Y")) # nb of NAs
      
      plots[[sce]] <- annotate_figure(plot, 
                                      top = paste("\n Scenario:", sce, 
                                                  "\n", scaff_outl, "ouliers in",
                                                  nrow(outlier_zoom), 
                                                  "windows"))
      
      
    }
    
    interest_scaff <- append(interest_scaff, s)
    plot <- ggarrange(plotlist = plots, ncol = 2, nrow = 2)
    print(annotate_figure(plot, top = paste("\nScaffold", s)))
    ggsave(paste0("../Results/Genome scans/ch01_GS_Outl_", s, ".pdf"), 
           height = 15, width = 10)
    
  }
  
}
  

# compare to genome size
# get genome info
# cat wtdgenome1.fasta | grep -c ">"
# cat wtdgenome1.fasta | grep -v ">" | wc -m
# genome with 2'546'246'737 bp = 2.5 Gbp in 2910 scaffolds

# ratio of outlier windows in background selection
# nb of outliers in BS / total number of windows *100
table(outlier_windows$scenario)[2]/sum(scaffold_size$end/50000)*100
# ratio of outlier windows in background selection
# nb of bp in BS / total genome lenght *100
table(outlier_windows$scenario)[2]*50000/2546246737*100

# ratio of outlier windows in balancing selection
table(outlier_windows$scenario)[3]/sum(scaffold_size$end/50000)*100
# ratio of outlier windows in balancing selection
table(outlier_windows$scenario)[3]*50000/2546246737*100
table(outlier_windows$scenario)[1]*50000/2546246737*100


```

#### annotation and GO analysis

Only on scaffold of interest = Scaffolds with > 75% of outlier windows across the whole scaffold (larger than 3 windows) or scaffolds with over 15 outlier windows

```{r eval=FALSE, message=FALSE, warning=FALSE, include=FALSE}
## for all windows with outliers, get scaffolds ID in a table --> whole scaffold
annotation_BS <- unique(outlier_windows[outlier_windows$scenario == "Balancing selection", 1])
write.table(annotation_BS, 
            file = "../Results/Genome scans/Scaffolds_balacingS.txt", 
            quote = FALSE, sep = "\t", row.names = FALSE, col.names = FALSE)

annotation_BackgroundS <- unique(outlier_windows[outlier_windows$scenario == "Background selection", 1])
write.table(annotation_BackgroundS, 
            file = "../Results/Genome scans/Scaffolds_backgroundS.txt", 
            quote = FALSE, sep = "\t", row.names = FALSE, col.names = FALSE)

## scaffolds ID, start & end in a table
annotation_BS <- unique(outlier_windows[outlier_windows$scenario == "Balancing selection", 1:3])
write.table(annotation_BS, 
            file = "../Results/Genome scans/OutlierWin_BalancingS.txt", 
            quote = FALSE, sep = "\t", row.names = FALSE, col.names = FALSE)

annotation_BackgroundS <- unique(outlier_windows[outlier_windows$scenario == "Background selection", 1:3])

write.table(annotation_BackgroundS, 
            file = "../Results/Genome scans/OutlierWin_BackgroundS.txt", 
            quote = FALSE, sep = "\t", row.names = FALSE, col.names = FALSE)


```

get gene IDs from the gtf

```{bash echo=T, eval=F}
# upload r dataset
rsync -vaP Desktop/Dropbox/PhD/CH_01/Results/Genome\ scans/*.txt ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/genome_scans/martin/

graham
annot=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files/ANNOTATION/
ref=/home/ckessler/projects/rrg-shaferab/ckessler/GENOME_files/
cd projects/rrg-shaferab/ckessler/chapter01/genome_scans/martin/


### on whole scaffolds
# use of >> ro make sure it doesn't already exist
### on whole scaffolds
# use of >> ro make sure it doesn't already exist
rm Scaffolds_geneIDsBackgroundS.txt 
rm Scaffolds_geneIDsBalancingS.txt 

# create file
touch Scaffolds_geneIDsBackgroundS.txt 
touch Scaffolds_geneIDsBalancingS.txt 
# loop to get only the gene IDs in each scaffold
while read line
do
echo $line >> Scaffolds_geneIDsBackgroundS.txt
cat $annot/genes.gtf | grep $line | awk -F "\t" '{print $9}' | awk -F ";" '{print $2}' | uniq >> Scaffolds_geneIDsBackgroundS.txt
done < Scaffolds_backgroundS.txt

while read line
do
echo $line >> Scaffolds_geneIDsBalancingS.txt
cat $annot/genes.gtf | grep $line | awk -F "\t" '{print $9}' | awk -F ";" '{print $2}' | uniq >> Scaffolds_geneIDsBalancingS.txt
done < Scaffolds_balacingS.txt

# format geneIDs to be able to upload in ShinyGO
# delete lines with chromosom number, "geneID" and WTD... whic are not recognisable
cat Scaffolds_geneIDsBackgroundS.txt | sed '/ref.*$/d' | sed 's/^ gene_id //' | sed 's/\"//g' | sed '/WTD*/d' > Scaffolds_cleangeneIDs_BackgroundS.txt
cat Scaffolds_geneIDsBalancingS.txt | sed '/ref.*$/d' | sed 's/^ gene_id //' | sed 's/\"//g' | sed '/WTD*/d' > Scaffolds_cleangeneIDs_BalancingS.txt

# loop to get all info for each scaffold
rm Scaffolds_allinfo_BackgroundS.txt 
touch Scaffolds_allinfo_BackgroundS.txt 

rm Scaffolds_allinfo_BalancingS.txt 
touch Scaffolds_allinfo_BalancingS.txt 

while read line
do
cat $annot/wtd_ME_AUGUSTUS.all.gff | grep $line >> Scaffolds_allinfo_BackgroundS.txt
done < Scaffolds_backgroundS.txt


while read line
do
cat $annot/wtd_ME_AUGUSTUS.all.gff | grep $line >> Scaffolds_allinfo_BalancingS.txt
done < Scaffolds_balacingS.txt

## on outlier windows only
# get info from sites within windows for intra and inter comparisons
module load bedtools
bedtools intersect -a Scaffolds_allinfo_BackgroundS.txt -b OutlierWin_BackgroundS.txt > OutlierWin_info_BackgroundS.txt
bedtools intersect -a Scaffolds_allinfo_BalancingS.txt -b OutlierWin_BalancingS.txt > OutlierWin_info_BalancingS.txt

# get clean gene ids
cat OutlierWin_info_BalancingS.txt | awk -F "\t" '{print $9}' | awk -F ";" '{print $2}' | sed 's/Parent=//' | sed 's/-RA//' | sed '/ref.*$/d' | sed '/^Name=.*$/d'| sed '/^WTD.*$/d' | uniq > OutlierWin_cleanGeneIDs_BalancingS.txt

cat OutlierWin_info_BackgroundS.txt | awk -F "\t" '{print $9}' | awk -F ";" '{print $2}' | sed 's/Parent=//' | sed 's/-RA//' | sed '/ref.*$/d' | sed '/^Name=.*$/d'| sed '/^WTD.*$/d' | uniq > OutlierWin_cleanGeneIDs_BackgroundS.txt

# get on local
rsync -vaP ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/genome_scans/martin/*clean* /Users/camillekessler/Desktop/Dropbox/PhD/CH_01/Results/Genome\ scans/

rsync -vaP ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/genome_scans/martin/*outliers_cleanIDs.txt /Users/camillekessler/Desktop/Dropbox/PhD/CH_01/Results/Genome\ scans/

# copy paste in shinyGO & uniprot


## allopatric selection
nano # ref0001398	8900001	8950000
#ref0002179	14700001	14750000
bedtools intersect -a $annot/genes.gtf -b OutlierWin_allopatricSelection.txt > OutlierWin_info_AllopatricS.txt
# Q9Y5E5 & Q15057, upload in uniprot

```


##### Zoom in outlier regions

```{r message=FALSE, warning=FALSE, fig.height=30, fig.width=10}
ShinyGO_outl_files <- list.files(path = "../Results/Genome scans", 
                                 pattern = "OutlierWin_GO*", 
                                 full.names = TRUE)

outlier_GOs <- list()
scenario <- c("BackgroundS", "BalancingS")
for (sce in scenario) {
  files <- grep(sce, ShinyGO_outl_files, value = TRUE)
  plots <- list()
  i = 0
  for (f in files) {
    
    GO <- gsub("^.*_GO", "", f)
    GO <- gsub("_Ba.*$", "", GO)
    name <- paste(sce, GO)
    # read each table
    outlier_GOs[[name]] <- read.csv(f, header = TRUE, 
                                    stringsAsFactors = FALSE)
      # plot at dotplot
      plots[[GO]] <- ggplot(outlier_GOs[[name]], 
                            aes(x = Fold.Enrichment, 
                                y = reorder(Pathway, Fold.Enrichment), 
                                colour = -log10(Enrichment.FDR), 
                                size = nGenes)) +
        geom_point() + 
        scale_color_gradient(low = "blue", high = "red") +
        labs(x = "Fold enrichment", y = GO, colour = "-log10 FDR", 
             title = paste("Enrichment analysis for", GO, "GOs")) +
        theme_minimal()
      i = i + 1
  }
  
  p <- ggarrange(plotlist = plots, ncol = 1, nrow = i, common.legend = TRUE, 
                 legend = "bottom")
  annotate_figure(p, top = paste("GEA on outlier windows from", sce, "scenario"))
  ggsave(paste0("../Results/Plots/ch01_GS_GO_Outl_",
                sce, ".pdf"), height = 22, width = 12)
}

```

*Wordcloud on geneID keywords generated by UniProt, genes from outlier windows in interspecies comparisons*
```{r message=FALSE, fig.height=15, fig.width=10}
## word cloud form uniprot
library(wordcloud) 
library(readxl)
library(wesanderson)
library(viridis)
unirot_files <- list.files(path = "../Results/Genome scans", 
                           pattern = "*UNIPROT*", full.names = TRUE)
for (f in unirot_files) {
  # create df with word in col1 and freq in col2
  uniprot_outliers <- read_excel(f)
  scenario <- gsub("^.*PROT_", "", f)
  scenario <- gsub(".xlsx", "", scenario)

  # get the list of words
  uniprot_outliers <- strsplit(uniprot_outliers$Keywords, split = ";")
  uniprot_outliers <- unlist(uniprot_outliers)
  # use table as df
  uniprot_count <- as.data.frame(table(uniprot_outliers))
  # plot it
  pdf(paste0("../Results/Plots/ch01_GS_GO_Outl",
             scenario, "_wordcloud.pdf"))
  wordcloud(words = uniprot_count$uniprot_outliers, 
            freq = uniprot_count$Freq,
            rot.per = 0.35, scale = c(8,.3), random.order = TRUE, 
            colors = viridis(option = "C", n = 50), 
            vfont = c("sans serif", "plain"))
  dev.off()
  
}

```


```{r}
sessionInfo()
```


