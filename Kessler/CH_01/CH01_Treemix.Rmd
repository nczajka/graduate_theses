---
title: "Treemix"
author: C. Kessler
date: "`r format(Sys.time(), '%d %B %Y')`"
output:
  html_document:
    theme: cosmo
    highlight: kate
    toc: true
    toc_depth: 4
    toc_float: true
    code_folding: show
editor_options: 
  chunk_output_type: console
---



# treemix

based on https://github.com/joepickrell/pophistory-tutorial & https://speciationgenomics.github.io/Treemix/

```{bash eval=FALSE}
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
## Set up 
ch01=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01
mkdir treemix
treemix=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/treemix
data=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/data
cariboudata=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/data_alignedCaribou
# get python scripts
wget https://github.com/evodify/genotype-files-manipulations/raw/master/calls_to_treeMix_input.py
wget https://github.com/evodify/genotype-files-manipulations/raw/master/calls.py

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
## input file preparation
# prepare geno file
header1="#CHROM POS" # first part of header
header2=$(cat $data/pops_namevcf.txt | cut -f 1 | paste -s) # get individual names
echo -e $header1"\t"$header2 | sed 's/ \+/\t/g' > header # concatenate

cat header $data/ch01_deer_angsd.geno > $data/ch01_deer_angsd_compatible_named.geno # add to .geno

#get each population idividuals
cat $data/pops_namevcf.txt | awk '{print $2}' | uniq
cat $data/pops_namevcf.txt | grep "WTD_allopatry" | awk '{print $1}' | tr '\n' ','
cat $data/pops_namevcf.txt | grep "WTD_sympatry" | awk '{print $1}' | tr '\n' ','
cat $data/pops_namevcf.txt | grep "MD_allopatry" | awk '{print $1}' | tr '\n' ','
cat $data/pops_namevcf.txt | grep "MD_sympatry" | awk '{print $1}' | tr '\n' ','

# convert named geno file to treemix input
module load python/2.7.18
sbatch -o slurms/geno2treemix-%A.out --account=rrg-shaferab --mem=1G --time=02:00:00 --job-name=geno2treemix --wrap="python calls_to_treeMix_input.py -i $data/ch01_deer_angsd_compatible_named.geno -o ch01_deer_angsd_geno2treemix.out -p 'WTD_allopatry[Odo_ON3,Odo_ON5,Odo_ON6_merged_dowsampled,Odo_PA3,Odo_VA3,Odo_AL3,Odo_SC1];WTD_sympatry[Odo_AB1,Odo_SK2,Odo_SK3,Odo_SD1,Odo_KS1,Odo_MT1,Odo_MX2];MD_allopatry[MD_NV2,MD_CA3,MD_BC6,MD_BC8,BTD_BC1,SD_AK1,BTD_WA1_merged_downsampled];MD_sympatry[MD_AB1,MD_UT2,MD_CO1,MD_SK5,MD_OR2,MD_SD2,MD_NM2_merged_downsampled]'"

# need zipped input
gzip -c ch01_deer_angsd_geno2treemix.out > ch01_deer_angsd_geno2treemix.out.gz

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
## treemix
module load StdEnv/2020 gcc/9.3.0 treemix

for i in {0..5}
do
sbatch -o slurms/treemix_deer-%A.out --account=rrg-shaferab --cpus-per-task 4 --mem=16G --time=06:00:00 --job-name=treemix_deer --wrap="treemix -i ch01_deer_angsd_geno2treemix.out.gz -m $i -o ch01_deer_angsd_m$i -root WTD_allopatry -bootstrap -k 1000 -se"
done

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
# fourpopulation test
sbatch -o slurms/fourpop_deer-%A.out --account=rrg-shaferab --cpus-per-task 3 --mem=12G --time=71:00:00 --job-name=fourpop_deer --wrap="fourpop -i ch01_deer_angsd_geno2treemix.out.gz -k 1000 > ch01_deer_angsd_4poptest.txt"
# ERROR: Line 50544312 has 2 entries. Header has 4
sed -n '50544311,50544313p' ch01_deer_angsd_geno2treemix.out

sbatch -o slurms/fourpop_deer-%A.out --account=rrg-shaferab --cpus-per-task 2 --mem=8G --time=671:00:00 --job-name=fourpop_deer --wrap="fourpop -i ch01_deer_angsd_geno2treemix.out.gz -k 1000 > ch01_deer_angsd_4poptest.txt"

# 3population test
sbatch -o slurms/threepop_deer-%A.out --account=rrg-shaferab --cpus-per-task 2 --mem=8G --time=671:00:00 --job-name=threepop_deer --wrap="threepop -i ch01_deer_angsd_geno2treemix.out.gz -k 1000 > ch01_deer_angsd_3poptest.txt"

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
## treemix with caribou
# prepare input
gunzip $cariboudata/ch01_angsd_mapped_caribou_named.geno.gz
head $cariboudata/ch01_angsd_mapped_caribou_named.geno

module load python/2.7.18
sbatch -o slurms/geno2treemix_alignedCaribou-%A.out --account=rrg-shaferab --mem=1G --time=02:00:00 --job-name=geno2treemix --wrap="python calls_to_treeMix_input.py -i $cariboudata/ch01_angsd_mapped_caribou_named.geno -o ch01_angsd_mapped_caribou_geno2treemix.out -p 'WTD_allopatry[Odo_ON3,Odo_ON5,Odo_ON6,Odo_PA3,Odo_VA3,Odo_AL3,Odo_SC1];WTD_sympatry[Odo_AB1,Odo_SK2,Odo_SK3,Odo_SD1,Odo_KS1,Odo_MT1,Odo_MX2];MD_allopatry[MD_NV2,MD_CA3,MD_BC6,MD_BC8,BTD_BC1,SD_AK1,BTD_WA1];MD_sympatry[MD_AB1,MD_UT2,MD_CO1,MD_SK5,MD_OR2,MD_SD2,MD_NM2];Caribou[GAP-157]'"

# need zipped input
gzip -c ch01_angsd_mapped_caribou_geno2treemix.out > ch01_angsd_mapped_caribou_geno2treemix.out.gz

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
# treemix
for i in {0..5}
do
sbatch -o slurms/treemix_wCaribou-%A.out --account=rrg-shaferab --cpus-per-task 4 --mem=16G --time=06:00:00 --job-name=treemix_caribou --wrap="treemix -i ch01_angsd_mapped_caribou_geno2treemix.out.gz -m $i -o ch01_angsd_mapped_caribou_m$i -root Caribou -bootstrap -k 1000 -se"
done

# fourpopulation test
sbatch -o slurms/fourpop_caribou-%A.out --account=rrg-shaferab --cpus-per-task 2 --mem=8G --time=671:00:00 --job-name=fourpop_caribou --wrap="fourpop -i ch01_angsd_mapped_caribou_geno2treemix.out.gz -k 1000 > ch01_angsd_mappedCaribou_4poptest.txt"

# 3population test
sbatch -o slurms/threepop_caribou-%A.out --account=rrg-shaferab --cpus-per-task 2 --mem=8G --time=671:00:00 --job-name=threepop_caribou --wrap="threepop -i ch01_angsd_mapped_caribou_geno2treemix.out.gz -k 1000 > ch01_angsd_mappedCaribou_3poptest.txt"

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
# get data on local
rsync -vaP ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/treemix/results /Users/camillekessler/Desktop/Dropbox/PhD/CH_01/Results/treemix/results

rsync -vaP ckessler@graham.computecanada.ca:/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/treemix/*.txt /Users/camillekessler/Desktop/Dropbox/PhD/CH_01/Results/treemix
```

## treemix
```{r message=FALSE, warning=FALSE, results='hide', fig.height=15}
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
# import
prefix <- c("ch01_angsd_mapped_caribou", "ch01_deer_angsd")
path <- "../Results/treemix/results/"
# manually create pops.txt with one population per line
source("plotting_funcs.R")
library(plotfunctions)

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
#plots
for (p in prefix) {
  data_name <- ifelse(grepl(pattern = "caribou", x = p), 
                      yes = print("incl.Caribou"), 
                      no = print("deer"))
  if (data_name == "deer") {
    cat("\n\n\n### Deer only data\n\n")
  } else {
    cat("\n\n\n### Deer mapped to Caribou\n\n")
  }
  for(mig in 0:5){
    pdf(file = paste0("../Results/Plots/ch01_treemix_", data_name, "_mig", mig),
        height = 12)
    par(mfrow = c(3, 1))
    plot_tree(paste0(path, p, "_m", mig), plus = 0.06, xmin = -0.02, 
              lwd = 3, arrow = 0.3, mbar = FALSE)
    title(paste("Data:", data_name, "for", mig,"migrations"))
    gradientLegend(valRange = c(0, 0.5), 
                   color = rev(heat.colors(150))[50:150], 
                   pos = c(0, 0.1, 0.02, 0.5))
    if (grepl("caribou", p)) {
      plot_cov(stem = paste0(path, p, "_m", mig), 
               pop_order = "../Data/pops_Caribou.txt", cex = 0.5)
      title("\n\n\nVariance plot")
      plot_resid(stem = paste0(path, p, "_m", mig),
                 pop_order = "../Data/pops_Caribou.txt", cex = 0.5)
      title("\n\n\nResiduals plot")
    } else {
      plot_cov(stem = paste0(path, p, "_m", mig), 
               pop_order = "../Data/pops_deer.txt", cex = 0.5)
      title("\n\n\nVariance plot")
      plot_resid(stem = paste0(path, p, "_m", mig),
                 pop_order = "../Data/pops_deer.txt", cex = 0.5)
      title("\n\n\nResiduals plot")
    }
    dev.off()
  }
}

##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
# https://github.com/mmabry/Brassica-oleracea-Population-and-Phylogenetics#12-treemix-httpsspeciationgenomicsgithubiotreemix
##check how much variance is explained 
library(ggplot2)
for (p in prefix) {
  var <- c()
  for (i in 0:5) {
    var[i + 1] <- get_f(paste0(path, p, "_m", i)) # get variance for each migration
  }
  MigrationEdges <- c("0", "1", "2", "3", "4", "5")
  varTable <- data.frame(MigrationEdges, var) # put in a table
  plots <- ggplot(varTable, aes(x = MigrationEdges, y = var, group = 1)) + 
    geom_point(shape = 21, color = "black", fill = "#69b3a2", size = 6) +
    geom_line() + ylim(0.99999, 1) +
    theme_minimal() +
    labs(x = "Migrations", y = "Variance explained", title = paste(p))
  print(plots)
  ggsave(paste0("../Results/Plots/ch01_treemix_variance", 
                p, ".pdf"))
}




```



## $f_3$ & $f_4$ statistics

if all three permutations of f4-statistics for a certain set of four populations are (significantly) non-zero, then at least one of the populations must be admixed (https://reich.hms.harvard.edu/sites/reich.hms.harvard.edu/files/inline-files/preprints202003.0237.v2.pdf)

```{r message=FALSE, warning=FALSE, fig.width=10}
##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### 
# read data
path <- "../Results/treemix"
Fstat_files <- list.files(path = path, pattern = ".txt", full.names = TRUE)
library(ggplot2)
# loop through each file
Fstat_list <- list()
for (f in Fstat_files) {
  # format name
  name <- gsub(path, "", f)
  name <- gsub("/ch01_deer_angsd_", "Deer_", name)
  name <- gsub("/ch01_angsd_mapped", "", name)
  name <- gsub(".txt", "", name)
  # f3 or f4
  ftest_nb <- gsub(pattern = "^.*_", "f", name)
  ftest_nb <- gsub(pattern = "poptest", "", ftest_nb)
  # headers in the middle of data --> needs removing
  # read each line
  lines <- readLines(f)
  # extract lines to remove
  lines_rm <- grep("Estimating|total_nsnp", lines) 
  # read table without lines we don't want (the first one too)
  Fstat_list[[name]] <-  read.table(text = lines[-lines_rm], skip = 1)
  colnames(Fstat_list[[name]]) <- c("Comparison", ftest_nb, "stderr", "Zscore")
  # plot it
  p <- ggplot(Fstat_list[[name]], aes_string(x = ftest_nb, y = "Comparison", 
                                 colour = "Zscore")) +
    geom_point() + theme_minimal() +
    ggtitle(paste(ftest_nb, "statistic for data mapped to", gsub("_.*$", "", name)))
  print(p)
  ggsave(paste0("../Results/Plots/ch01_treemix_", name, ".pdf"),
         plot = p)


}

```

```{r}
sessionInfo()
```

