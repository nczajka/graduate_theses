###### Obtaining orthologs and estimating μ ###### 
#Mutation rate with PorthoMCL + Excel + PAML


# Data pre-processng, modify headers of fasta files 

for fasta in *.fasta; do
cut -f 1 -d " " $fasta > ${fasta%.*}.temp;
mv ${fasta%.*}.temp $fasta
done

###Filtering sequences

#nuber of AA in each scaffold 
#!/bin/bash

for r in *_pep.fa;
do
awk '/^>/ {if (seqlen) print seqlen;print;seqlen=0;next} {seqlen+=length($0)}END{print seqlen}' ${r} > ${r}_raw_count
done

#print only the numbers

#!/bin/bash

for r in *_raw_count;
do
grep -v ">"  ${r}| awk '{print $3$1}' > ${r}2
done


##filter each species_pep.fasta by >50 AA
https://github.com/shenwei356/seqkit

nano seq_loop
#!/bin/bash
For r in *.fasta
do
     seqkit seq -m 50 ${r} > ${r}_filtered.fasta
done

#seqkit seq -m 50 species_pep.fasta > species_pep_filtered.fasta
#this will filter each fasta to only keep scaffolds with >50 AA

####### PorthoMCL ######
#PorthoMCL to find orthologs

https://github.com/etabari/PorthoMCL/wiki/Manual


#modify headers of fasta files to only the species name

for fasta in *.fasta; do
cut -f 1 -d " " $fasta > ${fasta%.*}.temp;
mv ${fasta%.*}.temp $fasta
done

#https://github.com/etabari/PorthoMCL/wiki/Manual
git clone https://github.com/etabari/PorthoMCL
#the following modules need to be loaded. Can put these in your scripts or run them each time connecting 
module load perl/5.22.4
module load python/2.7.14
module load gcc/7.3.0 blast+/2.10.0
module load gcc/7.3.0 mcl/14.137

#####Step 1
#adjusting the fasta files to have the species base name at the header of each scaffold
mkdir -p original complaintFasta
mv *.fasta original/
cd complaintFasta
for fasta in ../original/*.fasta; 
do
	PorthoMCL/orthomclAdjustFasta $(basename ${fasta%.*})  4
done


#####Step 2
#making a file with good proteins and bad ones 
PorthoMCL/orthomclFilterFasta 1.complaintFasta 50 20
#output is goodproteins.fasta and poorproteins.fasta
mkdir 2.filteredFasta
mv goodProteins.fasta 2.filteredFasta/
mv poorProteins.fasta 2.filteredFasta/

#####Step 3
#3.1 Create BLAST database
makeblastdb -in 2.filteredFasta/goodProteins.fasta  -dbtype prot

mkdir 3.blastdb

mv 2.filteredFasta/goodProteins.fasta.* 3.blastdb/

#3.2 split the fasta files. BLASTP takes a very long time so this way it will run much faster
PorthoMCL/porthomclSplitFasta.py -i 2.filteredFasta/goodProteins.fasta  -o 3.blastquery

#3.3 run blasts
nano 3blastp.sh
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=blastp_run
#SBATCH --ntasks-per-node=32
#SBATCH --mem=48G
#SBATCH --time=48:00:00

mkdir 3.blastres

module load gcc/7.3.0 blast+/2.10.0

for query in 3.blastquery/*
do
   strand=$(basename "$query")
   strand="${strand%.*}" # remove .fasta
   blastp -query 3.blastquery/$strand.fasta  -db 3.blastdb/goodProteins.fasta  -seg yes  -dbsize 100000000  -evalue 1e-5  -outfmt 6 -num_threads 32 -out 3.blastres/$strand.tab
done

#####Step 4

nano 4porthomclBlastParser.sh

#!/bin/bash
#SBATCH --time=00-06:00
#SBATCH --account=rrg-shaferab
#SBATCH --ntasks-per-node=16
#SBATCH --job-name=porthomclBlastParser
#SBATCH --output=%x-%j.out

mkdir 4.splitSimSeq

for blres in sample/3.blastres/*
do
    strand=$(basename "$blres")
    strand="${strand%.*}" # remove .tab
    PorthoMCL/porthomclBlastParser 3.blastres/$strand.tab 1.complaintFasta/ >> 4.splitSimSeq/$strand.ss.tsv
done

#####Step 5
#must run for each species in taxon list (x 1-12)
#can write a loop to do this but only takes a few minutes each so i just ran individually while changing the number for -x

mkdir 5.paralogTemp
mkdir 5.besthit

PorthoMCL/porthomclPairsBestHit.py -t taxon_list -s 4.splitSimSeq -b 5.besthit -q 5.paralogTemp --percentMatchCutoff 50 -x 1 

####Step 6

#create a list of best hits 
mkdir 6.orthologs

PorthoMCL/porthomclPairsOrthologs.py -t taxon_list -b 5.besthit/ -o 6.orthologs -x 1

####step 7
# -I Sets  the main inflation value. Ranges from 1.2-5.0, 5.0 being the most fine grained

cat 6.orthologs/*.tsv >> 8.all.ort.tsv

mcl 8.all.ort.tsv  --abc -I 1.5 -t 4 -o 8.all.ort.group

####Step 8 

python porthomcl_parser.py -i 8.all.ort.group -t taxon_list -min 7

#the program changed output to out.mcl
#copy the ortholog list to desktop to explore
scp -r Graham:/home/modedato/working/mutation_rate/PorthoMCL_test/out.mcl ./


###### Ortholog list modifications ######

#using perl script from Beginners guide to calculating dN/dS https://figshare.com/projects/Beginners_guide_to_caclulating_dN_dS/16032 
# Highly suggest finding a new method for aligning orthologs, this was very complicated 


#must modify the ortholog list to match the mandatory inputs for the alignment step
#inputs include:
#1)A CSV list of groups of orthologs, where line corresponds to an orthologous group,
#		and each ortholog within each group (i.e. line) is separated by commas.
#2) Corresponding DNA and Protein sequences for each ortholog, in FASTA format. Each named species#.prot.fasta and
# species#.cds.fasta 
#specifics found at 

#In excel, isolate the orthologous pep sequence name (get rid of the species name that is infront of it)
=RIGHT(A2, LEN(A2)-1)
#Then can make a lst of those sequence names in unix 
nano species_pep_orthos

#since PorthoMCL only gives pep orthologs, must find the corrosponding CDS sequences for those orthologous genes
#1. get the list of genes from the ortholog proteins

#!/bin/bash

for p in $(cat species_pep_orthos);
do
    grep 'gene:' ./species_split_pep/$p | head -1  >> species.out.1 
done

#get the list down to only the gene name 
cut -d ' ' -f4 file.out.1 >> file.out.2

#2.find the CDS that belong to those ortholog genes 

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --ntasks-per-node=16
#SBATCH --mem=48G
#SBATCH --time=0-06:00

for p in $(cat species.out.2);
do
    grep $p *.fasta -m 1 | head -1  >> species.out.3 
done

#3. create a list of those CDS fasta so you can rename to the required names for the input
cut -d ':' -f1 file.out.3 >> file.out.4

#rename the list in file.out.4 to match the required species#.cds.fasta
#eg 
#cp	ENSBTAT00000015633.6.fasta	cow1.dna.fasta
#cp	ENSBTAT00000003218.6.fasta	cow2.dna.fasta
#cp	ENSBTAT00000060954.3.fasta	cow3.dna.fasta
#cp	ENSBTAT00000010108.3.fasta	cow4.dna.fasta

#now have final input for the alinging orthologs step:
#species_list.csv
#species#.prot.fasta files
#species#.dna.fasta files

#Now run align script

#######align_orthologs.sh######

#1. #renaming the gene (and first line) to say just the species name and then the sequence (align_orthologs.pl gets mad if there are inconsistancies)
nano rename.bash
for file in $(ls cow*);
do
sed 1d $file >$file.mod
sed -i '1i >cow' $file.mod
mv $file.mod $file
done

#2. run align_orthologs.pl script 
nano align_orthologs.sh

#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --mem 64G
#SBATCH --job-name=align_run
#SBATCH --ntasks-per-node=32 
#SBATCH --time=0-6:00
#SBATCH --output=%x-%j.out

module load bioperl/1.7.5
module load clustal-omega
module load gcc/7.3.0 blast+

mkdir alignments_final
perl align_orthologs.pl -l species_list.csv -i align_input / -o alignments_final / -c -n 

###### RAxML to get tree ######
mkdir tree_run

#1. convert paml to fasta

#FASTA file for RAXML
#Makes a concatenated fasta file of the new aligned sequences generated for paml called .fasta - change as needed
#step 1 - convert paml to fasta files; works for current names

for file in $(ls *paml);
do
	sed 1d $file > $file.fasta
	sed -i 's/cow/>cow/g' $file.fasta
	sed -i 's/gemsbok/>gemsbok/g' $file.fasta
	sed -i 's/goat/>goat/g' $file.fasta
	sed -i 's/milu/>milu/g' $file.fasta
	sed -i 's/reindeer/>reindeer/g' $file.fasta
	sed -i 's/sheep/>sheep/g' $file.fasta
	sed -i 's/white_tailed_deer/>white_tailed_deer/g' $file.fasta
done

#step 2 - concatenate fasta files
#download fasConCat - will recognized and merge all files with the same name in the fasta file
git clone https://github.com/PatrickKueck/FASconCAT-G.git
perl FASconCAT-G/FASconCAT-G_v1.04.pl -s
#output is named FcC_supermatrix.fas

#step 3 - convert concatenated fasta to phylip for RAxML

#named new file FcC_supermatrix.phy
perl Fasta2Phylip.pl FcC_supermatrix.fas FcC_supermatrix.phy

#step 4 - download and run RAxML
#opted to download RAxML becuase server version has a small bug but works still
git clone https://github.com/stamatak/standard-RAxML.git
cd standard-RAxML
make -f Makefile.SSE3.PTHREADS.gcc
cd ..
​
# run the search for the "best" ML tree 
nano raxml_tree.sh
./standard-RAxML/raxmlHPC-PTHREADS-SSE3\
    -m GTRGAMMA \
    -p 1349 \
    -N 100 \
    -o cow
    -n BEST \
    -s FcC_supermatrix.phy 
    
#Output for codeml: RAxML_bestTree.BEST
((milu:0.05864285669099086884,(((goat:0.01058807988913605842,sheep:0.02583824980062918858):0.00519780273808279070,gemsbok:0.03485606149268474091):
  0.00728339908700905304,cow:0.02343954636784860435):0.01479983568316454133):0.00691298887896360948,white_tailed_deer:0.01142838491335250797,
  reindeer:0.03893911150355824219)

#now to calibrate it with known split times using mcmctree
git clone https://github.com/abacus-gene/paml
cd paml/src
make -f Makefile
#The time unit of numbers added is 100 Million years (Myr)
nano tree_input_nodetimes.tree

7 1
(((((goat,sheep)'>.039<.081',gemsbok),cow)'>.183<.285',((white_tailed_deer,reindeer),milu)),horse);
//end of file
(((((goat,sheep) 'B(.039, .081)',gemsbok),cow) 'B(.183, .285)',((white_tailed_deer,reindeer),milu)),horse);


###### mcmctree ######
#calibrate the nodes with known split times
nano mcmctree_RUN.sh

#!/bin/bash
#SBATCH --nodes=1
#SBATCH --tasks-per-node=4
#SBATCH --mem=16000
#SBATCH --time=0-08:00           # time (DD-HH:MM)
#SBATCH --account=rrg-shaferab
#SBATCH --output=%x_%j.out

/home/modedato/bin/paml/src/mcmctree mcmctree.ctl

#only used ~1h30m

# calibrated on cow-goat and sheep-goat nodes
cat tree_input_nodetimes.tree
7 1
(((((goat,sheep)'>.039<.081',gemsbok),cow)'>.183<.285',((white_tailed_deer,reindeer),milu)),horse);
//end of file
(((((goat,sheep) 'B(.039, .081)',gemsbok),cow) 'B(.183, .285)',((white_tailed_deer,reindeer),milu)),horse);


cat mcmctree_node.ctl
# seed            = -1
# seqfile         = FcC_supermatrix.fas
# treefile        = tree_input_nodetimes.tree
# mcmcfile        = mcmc_nodes.txt
# outfile         = out_nodes.txt
# ndata           = 1
# seqtype         = 2     *0:nucleotides;1:codons;2:AAs
# usedata         = 1             *0:no data;1:seq; 2:approximation; 3:out.BV(in.BV)
# clock           = 2             *1:global clock; 2:independent; and 3:correlated rates
# RootAge         = '<1.3'        *safe constraint on root age,used if no fossil for root.
# model           = 0             *0:JC69,1:K80,2:F81,3:F84,4:HKY85
# alpha           = 0             *alpha for gamma rate satsites
# ncatG           = 5             *No. categories in discrete gamma
# cleandata       = 0             *remove sites with ambiguity data(1:yes,0:no)?
# BDparas         = 1 1 0.1               *birth,death,sampling
# kappa_gamma     = 6 2           *gamma prior for kappa
# alpha_gamma     = 1 1           *gamma prior for alpha
# rgene_gamma     = 2 20 1                *gamma Dir prior for rate for genes
# sigma2_gamma    = 1 10 1                *gamma Dir prior for sigma^2(forclock=2or3)
# finetune        = 1: .1 .1 .1 .1 .1 .1  *auto(0or1):times,rates,mixing...
# print           = 1             *0:nomcmcsample;1:everythingexceptbranch2:ev...
# burnin          = 5000
# sampfreq        = 10
# nsample         = 50000

#mcmctree creates a figtree output in the "out_node.txt". Use this for figtree

###### FIGTREE VIEWER ######
#NEXUS tree from mcmctree
BEGIN TREES;

  UTREE 1 = (((((goat: 0.072504, sheep: 0.072504) [&95%HPD={0.0546667, 0.0839481}]: 0.069796, gemsbok: 0.142300) [&95%HPD={0.0969553, 0.190367}]: 0.071112, cow: 0.213413) [&95%HPD={0.177035, 0.268262}]: 0.064615, ((white_tailed_deer: 0.136895, reindeer: 0.136895) [&95%HPD={0.0787275, 0.203589}]: 0.054125, milu: 0.191020) [&95%HPD={0.130611, 0.262777}]: 0.087008) [&95%HPD={0.210422, 0.362403}]: 0.303256, horse: 0.581284) [&95%HPD={0.267626, 0.957153}];

END;

#load into figtree viewer
#node labes - 95%HPD
#Node bars - 95%HPD
#branch labeles - branch times


######PAML/CODEML######

#1 convert concatenated fasta to paml 
#1.1 Find sequence length
cat FcC_supermatrix.fas  | awk '$0 ">" {c+=length($0);} END { print c; }'

#1.2
#Add number of sequences (no. of species 4 in this case) and the length (from above - 12130854)
#Note this will change with different species etc.
#AND paml output had three spaces before each number, included below"
(echo "   7   3603004" && cat FcC_supermatrix.fas) > FcC_supermatrix.paml
​
#1.3
#last thing to do, remove the > as these aren't in the .paml format
sed -i -e 's/>//g' FcC_supermatrix.paml

#2. Run PAML/CODEML
module load nixpkgs/16.09  intel/2018.3
module load paml/4.9h

#input for codeml is this control file:
codeml.ctl

#to 4un codelm:
/home/modedato/bin/paml4.9j/bin/codeml codeml.ctl

#results /home/modedato/working/mutation_rate_052020/tree_run


###### R scripts for figures ######

#View tree in R#
if (!requireNamespace("BiocManager", quietly = TRUE))
  install.packages("BiocManager")

BiocManager::install("ggtree")

library(tidyverse)
library(ggplot2)
library(ggtree)
library(pdftools)


tree <- read.tree("RAxML_bestTree.BEST")
tree

ggtree(tree)

a <- ggtree(tree) + theme_tree2()
pdf("phylogenetic_tree.pdf")
a + geom_tiplab() +
  geom_nodepoint(color = "purple", size = 3, alpha = 0.5)
dev.off()










​

