#Bear (Ursus americanus) methylation clock
#Main author: Joseph A. Zoller (from Steve Horvath's lab) 

#installing dependencies
install.packages("ggfortify")
install.packages("shape")
install.packages("RcppEigen")
install.packages("glmnet")
BiocManager::install("impute") 
BiocManager::install("GO.db")
install.packages("WGCNA") 
install.packages("survival")
install.packages("tab")
install.packages("grid")
install.packages("gridExtra")
install.packages("rmcorr")

#loading packages
options(stringAsFactors=F)
library(tidyverse)
library(ggplot2)
library(ggfortify)
library(glmnet)
library(WGCNA) #transposeBigData
library(survival)
library(tab) #tabcoxph
library(grid) #gtable
library(gridExtra) #grid.arrange
library(rmcorr)

##Setting project directories
setwd("~/Dropbox/Methylation_ALLIANCE/Natalie_Czajka_MSc")
#run R_functions_JZ.R before running the rest of this script (Author: Joseph Zoller (from Steve Horvath's lab))
source("~/Dropbox/Methylation_ALLIANCE/Natalie_Czajka_MSc/EpigeneticClocks/Scripts/R_functions_JZ.R")

################# Part 1: Loading and Setting Data

#Creating Data Sets
datBearSamp_tp.rdata <- paste0('Chapter1/ProbeNormalization/Normalized/Bear_AllProbes_normalized_minfi.Rdata') 
probes_BEAR <- read_csv("Chapter1/ProbeNormalization/Probes/probes_BEAR.csv") #species specific

#Loading sample sheet data
infoN15=read.csv('Chapter1/SampleSheetAll_CH1.csv') #is this actually necessary?
infoAllSamp=read.csv('Chapter1/SampleSheetAll_CH1.csv', as.is=T)
infoAllSamp <- infoAllSamp %>%
  dplyr::select(Basename,SpeciesLatinName,OriginalOrderInBatch,Age,ConfidenceInAgeEstimate,
                CanBeUsedForAgingStudies,Tissue,Female,SpeciesCommonName,ExternalSampleID,Folder)
if ("Female" %in% colnames(infoAllSamp)) {
  infoAllSamp$Female[which(is.na(infoAllSamp$Female))] <- "NA"
  infoAllSamp$Female <- factor(infoAllSamp$Female, levels=c(0,1,"NA"))
  levels(infoAllSamp$Female) <- c("Male","Female","NA")
}


#Loading reformatted DNA methylation data
load(datBearSamp_tp.rdata)
datBearSamp_tp=normalized_betas_minfi_bear
rm(datBearSamp_tp.rdata, normalized_betas_minfi_bear)
datBearSamp <- datBearSamp_tp %>% dplyr::select(-1) %>% t() %>% as.data.frame()
colnames(datBearSamp)=datBearSamp_tp$CGid
datBearSamp <- cbind(Basename=rownames(datBearSamp), datBearSamp)
rownames(datBearSamp)=seq_len(nrow(datBearSamp))
datBearSamp$Basename <- as.character(datBearSamp$Basename)
rm(datBearSamp_tp)

################# Part 2: Refining Data

infoCBUAllSamp <- infoAllSamp %>% dplyr::filter(CanBeUsedForAgingStudies=="yes") %>%
  dplyr::filter(ConfidenceInAgeEstimate>=90) %>%
  dplyr::filter(!is.na(Age)) %>%
  dplyr::filter(Basename %in% datBearSamp$Basename)

### Sample filtering and partitioning
infoAllBear <- infoAllSamp %>%
  dplyr::filter(SpeciesLatinName %in% c("Ursus americanus"))
#basically rename the whole dataset, only keeping the one that are good to use (CBU = only good to use samples, removed outliers)
infoCBUBear <- infoCBUAllSamp %>%
  dplyr::filter(SpeciesLatinName %in% c("Ursus americanus"))

#making latin, common name and tissue columns factors
infoCBUBear$SpeciesLatinName <- factor(infoCBUBear$SpeciesLatinName)
infoCBUBear$SpeciesCommonName <- factor(infoCBUBear$SpeciesCommonName)
infoCBUBear$Tissue <- factor(infoCBUBear$Tissue)

infoNBUBear <- infoAllBear %>% dplyr::filter(!Basename %in% infoCBUBear$Basename) %>% 
  dplyr::filter(CanBeUsedForAgingStudies == "yes")

#change columns to factor
infoNBUBear$SpeciesLatinName <- factor(infoNBUBear$SpeciesLatinName)
infoNBUBear$SpeciesCommonName <- factor(infoNBUBear$SpeciesCommonName)
infoNBUBear$Tissue <- factor(infoNBUBear$Tissue)

### Data refinement of CpG Sites, based on shared probe mappings
#remove all columns that don't have matching cgID as column name
datAllSamp_subCPGBear <- datBearSamp[,c(1,which(colnames(datBearSamp) %in% probes_BEAR$probeID))]

#latin name of black bear
table(as.character(infoCBUBear$Tissue))
latin2common_bear <- unique(dplyr::select(infoCBUBear,SpeciesLatinName,SpeciesCommonName))

#####USING ALIGNED PROBE DATA####
###Clock pre-analysis, using subsetted CpGs without outlier samples
#setting up training groups for analysis
set.seed(1236)
yxs.train.list <- alignInfoDat(infoCBUBear,datAllSamp_subCPGBear,"Basename","Basename")
yxs.test.list <- alignInfoDat(infoNBUBear,datAllSamp_subCPGBear,"Basename","Basename")
ys.train <- yxs.train.list[[1]]
xs.train <- yxs.train.list[[2]]
ys.test <- yxs.test.list[[1]]
xs.test <- yxs.test.list[[2]]
rm(yxs.train.list,yxs.test.list)

###Clock analysis, using subsetted CpGs without outlier samples , Training from All
#All outputs are "Based on All"
OUTVAR="Age"
out.csv='Bear_Clock_EpigeneticAge.csv'
output.csv='Bear_Clock_EpigeneticAge_PredictedValues.csv'
out.png='Bear_Clock_EpigeneticAge.png'
out.png.title='Bear_Clock_EpigeneticAge'
PREDVAR="DNAmAge"
RESVAR="AgeAccel"
RESinTestVAR="AgeAccelinTest"
ALPHA=0.5
NFOLD=10
#run analysis
ys.output <- saveNetTrained(xs.train,ys.train,xs.test,ys.test,OUTVAR,out.csv,output.csv,out.png,out.png.title,PREDVAR,RESVAR,RESinTestVAR,ALPHA,NFOLD)
#add output data to infoAllBear dataset 
ys.output <- base::merge(infoAllBear, ys.output[, c("Basename", "train", PREDVAR, RESVAR, RESinTestVAR)],
                         "Basename", all=T, sort=F)
ys.output <- base::merge(infoAllBear, ys.output[, c("Basename", "train", PREDVAR, RESVAR, RESinTestVAR)],
                         "Basename", all=T, sort=F)
write.table(ys.output,output.csv,sep=',',row.names=F,quote=F)

## Square-Root Transformed
#All outputs are "Based on All"
OUTVAR="Age"
out.csv='Bear_Clock_EpigeneticSqrtAge.csv'
output.csv='Bear_Clock_EpigeneticSqrtAge_PredictedValues.csv'
out.png='Bear_Clock_EpigeneticSqrtAge.png'
out.png.title='Bear_Clock_EpigeneticSqrtAge'
PREDVAR="DNAmAge"
RESVAR="AgeAccel"
RESinTestVAR="AgeAccelinTest"
ALPHA=0.5
NFOLD=10
ys.output <- saveNetTrained(xs.train,ys.train,xs.test,ys.test,OUTVAR,out.csv,output.csv,out.png,out.png.title,PREDVAR,RESVAR,RESinTestVAR,ALPHA,NFOLD,fun_trans=fun_sqrt_trans,fun_inv=fun_sqrt_inv)
ys.output <- base::merge(infoAllBear, ys.output[, c("Basename", "train", PREDVAR, RESVAR, RESinTestVAR)],
                         "Basename", all=T, sort=F)
ys.output <- base::merge(infoAllBear, ys.output[, c("Basename", "train", PREDVAR, RESVAR, RESinTestVAR)],
                         "Basename", all=T, sort=F)
write.table(ys.output,output.csv,sep=',',row.names=F,quote=F)

## Logarithm Transformed
#All outputs are "Based on All"
OUTVAR="Age"
out.csv='Bear_Clock_EpigeneticLogAge.csv'
output.csv='Bear_Clock_EpigeneticLogAge_PredictedValues.csv'
out.png='Bear_Clock_EpigeneticLogAge.png'
out.png.title='Bear_Clock_EpigeneticLogAge'
PREDVAR="DNAmAge"
RESVAR="AgeAccel"
RESinTestVAR="AgeAccelinTest"
ALPHA=0.5
NFOLD=10
ys.output <- saveNetTrained(xs.train,ys.train,xs.test,ys.test,OUTVAR,out.csv,output.csv,out.png,out.png.title,PREDVAR,RESVAR,RESinTestVAR,ALPHA,NFOLD,fun_trans=fun_log_trans,fun_inv=fun_log_inv)
ys.output <- base::merge(infoAllBear, ys.output[, c("Basename", "train", PREDVAR, RESVAR, RESinTestVAR)],
                         "Basename", all=T, sort=F)
ys.output <- base::merge(infoAllBear, ys.output[, c("Basename", "train", PREDVAR, RESVAR, RESinTestVAR)],
                         "Basename", all=T, sort=F)
write.table(ys.output,output.csv,sep=',',row.names=F,quote=F)
