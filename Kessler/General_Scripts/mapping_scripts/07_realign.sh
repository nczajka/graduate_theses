#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=realign
#SBATCH --cpus-per-task=8
#SBATCH --mem=32G
#SBATCH --time=0-24:00 # time (DD-HH:MM)

module load StdEnv/2016.4
module load gatk/3.8
module load sambamba
echo ${1}
java -Xmx30G -jar $EBROOTGATK/GenomeAnalysisTK.jar -T RealignerTargetCreator \
--use_jdk_deflater --use_jdk_inflater -nt 8 \
-R ${2}/wtdgenome1.fasta -I ${3}/${1}.unique_reads.bam -o ${3}/${1}.realign.intervals &&
java -Xmx30G -jar $EBROOTGATK/GenomeAnalysisTK.jar -T IndelRealigner \
--use_jdk_deflater --use_jdk_inflater \
-R ${2}/wtdgenome1.fasta -I ${3}/${1}.unique_reads.bam -targetIntervals ${3}/${1}.realign.intervals \
-o ${3}/${1}.realigned.bam &&
${4}/sambamba-0.7.0-linux-static flagstat --nthreads=8 ${3}/${1}.realigned.bam \
> ${3}/${1}.realigned.flagstat