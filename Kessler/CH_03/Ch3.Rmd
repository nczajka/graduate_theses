---
# Summary of Chapter 3

title: "PhD Chapter 3"
author: C.Kessler
date: "`r format(Sys.time(), '%d %B %Y')`"
output:
  html_document:
    theme: cosmo
    highlight: kate
    toc: true
    toc_depth: 4
    toc_float: true
editor_options: 
  chunk_output_type: console
---

<style>
body {
text-align: justify}
</style>

# About

_Odocoileus_ evolution and adaptation
The _Odocoileus_ genera appears in the fossil record in mid-Pliocene in Kansas. Along with three other deer genera, the ancestral _Odocoileus_ lived in North America during the late Pliocene and grew increasingly common. _Odocoileus_ was, however, the only group that survived Pleistocene and remained as the only deer genera in North America (Hewitt, 2011). Currently, there are two extant species belonging to _Odocoileus_: the white-tailed deer (_O. virginianus_; WTD) and mule deer (_O. hemionus_; MD). WTD is the most abundant ungulate in North America, with a density impacting vegetation and predator-prey dynamics (Frerker et al., 2014; Hewitt, 2011). Both species represent a high economic value as deer hunting-related activities, and represent an important cultural component of Indigenous communities across the whole range of the species. Indeed, along with providing for food even today, _Odocoileus_ used to be important species for trade, clothing, and held a special status in some ancient communities which involved, for example, rituals while hunting or the use of antlers as amulets (Hewitt, 2011; Peres and Altman, 2018). 

Two key questions form the basis of our _Odocoileus_ aDNA work: historical biogeography and adaptation. Previous molecular clock attempts at determining the divergence time between those two species provide a large range of nearly three million years for their divergence (750,000 y - 3.7 ma). Divergence time estimates based on the fossil record are complicated, both species are highly similar in morphology and only a few bones can distinguish species (Jacobson, 2004). Regardless, the previously established divergence time, despite high variance, all pre-date the Last Glacial Maximum (LGM). The impact of LGM on species diversity depends largely on the species and population characteristics (Shafer et al., 2010). Bottleneck events usually leave genetic signatures which range from overall low genetic diversity to accumulation of deleterious mutations affecting fitness or survival (Allendorf et al., 2010; Frankham, 2005). Several analyses of WTD bottlenecked populations, however, showed different outcomes: WTD populations of Missouri, Anticosti and St-John island all underwent bottlenecks but none show genetic signatures of such event (Budd et al., 2018; Fuller et al., 2020; Nelson et al., 2021). The hypothesis advanced by the authors are diverse: a large number of founder individuals, rapid population growth or high gene flow levels  (Budd et al., 2018; Fuller et al., 2020; Nelson et al., 2021). Another potential bottleneck cause would be the settlement of Europeans in Canada, an extreme example is that of the elk (Cervus elaphus canadensis), which was driven to extinction in eastern North America due to overhunting (Keller et al., 2015). Likewise, WTD have been heavily influenced across its range: first in the 17th century by Indigenous people who hunted the species to trade it with Europeans. And later, in the 19th century, by the Europeans themselves who combined WTD overhunting to that of its habitat destruction through widespread timber harvest (Hewitt, 2011). The question we wish to address with aDNA is whether we can find genetic signatures for these events in both _Odocoileus_ sp., and estimate to what extent European settlement and LGM impacted deer populations in northern America.

Additionally, we would like to investigate adaptation and selection in _Odocoileus_ given how extensive their ranges are. Comparing ancient and modern DNA in parallel is analogous to looking at the same species but at different time points. This chronology, while looking at signature of selection in the genome, leads to a better understanding of the ways natural selection shaped the species (Dehasque et al., 2020). Adding aDNA samples allows us to control for cofounding factors in selection analyses, allowing us to clearly differentiate background signals from true selective response  (Dehasque et al., 2020). Using aDNA also allows to detect signs of adaptation. For example, van der Valk et al. (2021) showed changes in protein-coding regions of the genome between samples at different time points, likely due to adaptation. Contemporary _Odocoileus_ exhibit a significant range expansion and thrive in a range of environments; analysis of aDNA should allow us to identify what genes and modifications have led to the success of modern deer.


**Questions:**

How and why did WTD become so predominant? 

Can we improve our understanding from Chapter 01 ?

What were the influences of the LGM & European settlement?


**Hypotheses:**

We expect to find differences in nucleotide sequence between past/present populations which would be a sign of adaptation.

We are also looking for evidence of bottleneck caused either by the LGM or the European settlement, possibly lower in WTD since the species showed a high genetic resilience in previous studies.


# Sampling

Total = x
•	10 Ancient
•	X modern (set from CH.2)

![Fig.1 Species sampling map](/Users/camillekessler/Desktop/Dropbox/PhD/Results/Samples_CH3_map.png)



# Analyses

Pairwise sequentially Markovian coalescent (PSMC) 
Heterozygosity
Fst
D
π
Tajima's D


# Methods








# References

Allendorf, F.W., Hohenlohe, P.A., and Luikart, G. (2010). Genomics and the future of conservation genetics. Nat Rev Genet 11, 697–709.

Budd, K., Berkman, L.K., Anderson, M., Koppelman, J., and Eggert, L.S. (2018). Genetic structure and recovery of white-tailed deer in Missouri: Genetic Structure of Deer in Missouri. Jour. Wild. Mgmt. 82, 1598–1607.

Churcher, C.S., and Peterson, R.L. (1982). Chronologic and Environmental Implications of a New Genus of Fossil Deer from Late Wisconsin Deposits at Toronto, Canada. Quat. Res. 18, 184–195.

Dehasque, M., Ávila‐Arcos, M.C., Díez‐del‐Molino, D., Fumagalli, M., Guschanski, K., Lorenzen, E.D., Malaspinas, A., Marques‐Bonet, T., Martin, M.D., Murray, G.G.R., et al. (2020). Inference of natural selection from ancient DNA. Evolution Letters 4, 94–108.

Environmental Commissioner of Ontario, Dianne Saxe (2018). Back to Basics, 2018 Environmental Protection Report.

Fisher, J.T., and Burton, A.C. (2021). Spatial structure of reproductive success infers mechanisms of ungulate invasion in Nearctic boreal landscapes. Ecol. Evol. 11, 900–911.

Frankham, R. (2005). Genetics and extinction. Biological Conservation 126, 131–140.

Frerker, K., Sabo, A., and Waller, D. (2014). Long-Term Regional Shifts in Plant Community Composition Are Largely Explained by Local Deer Impact Experiments. PLOS ONE 17.

Fuller, J., Ferchaud, A., Laporte, M., Le Luyer, J., Davis, T.B., Côté, S.D., and Bernatchez, L. (2020). Absence of founder effect and evidence for adaptive divergence in a recently introduced insular population of white‐tailed deer ( Odocoileus virginianus ). Mol Ecol 29, 86–104.

Haworth, S.E., Nituch, L., Northrup, J.M., and Shafer, A.B.A. (2021). Characterizing the demographic history and prion protein variation to infer susceptibility to chronic wasting disease in a naïve population of white‐tailed deer ( Odocoileus virginianus ). Evol Appl eva.13214.

Hewitt, D.G. (2011). Biology and management of white-tailed deer (Boca Raton: CRC Press).
Jacobson, J.A. Determining Human Ecology on the Plains Through the Identification of Mule Deer (Odocoileus hemionus) and White-tailed Deer (Odocoileus virginianus) Postcranial Material. 216.

Keller, B.J., Montgomery, R.A., Iii, H.R.C., and Jr, D.E.B. (2015). A review of vital rates and cause‐specific mortality of elk Cervus elaphus populations in eastern North America. Mammal Review 14.

Nelson, S.L., Taylor, S.A., and Reuter, J.D. (2021). An isolated white‐tailed deer ( Odocoileus virginianus ) population on St. John, US Virgin Islands shows low inbreeding and comparable heterozygosity to other larger populations. Ecol Evol ece3.7230.

Peres, T.M., and Altman, H. (2018). The magic of improbable appendages: Deer antler objects in the archaeological record of the American South. Journal of Archaeological Science: Reports 20, 888–895.
Rogers, R.L., and Slatkin, M. (2017). Excess of genomic defects in a woolly mammoth on Wrangel island. PLoS Genet 13, e1006601.

Schmelzer, I. (2020). Boreal caribou survival in a warming climate, Labrador, Canada 1996-2014. Global Ecology and Conservation 25.

Shafer, A.B.A., Cullingham, C.I., Côté, S.D., and Coltman, D.W. (2010). Of glaciers and refugia: a decade of study sheds new light on the phylogeography of northwestern North America. Molecular Ecology 19, 4589–4621.

Spiess, A.E., Curran, M.L., and Grimes, J.R. (1985). Caribou ( Rangifer Tarandus L.) Bones from New England Paleoindian Sites. North American Archaeologist 6, 145–159.
Timmermann, H.R., and Rodgers, A.R. (2015). THE STATUS AND MANAGEMENT OF MOOSE IN NORTH AMERICA – CIRCA 2015. ALCES VOL. 53, 22.

van der Valk, T., Pečnerová, P., Díez-del-Molino, D., Bergström, A., Oppenheimer, J., Hartmann, S., Xenikoudakis, G., Thomas, J.A., Dehasque, M., Sağlıcan, E., et al. (2021). Million-year-old DNA sheds light on the genomic history of mammoths. Nature 591, 265–269.







