## Chapter 01 sampling
## Random sampling of 28 individuals within four groups

library(readxl)
library(dplyr)
Odo_complete <- read_xlsx("../Data/Deer_Genome_samples.xlsx")
Odo_complete <- as.data.frame(Odo_complete)
# remove subspecies from species names
Odo_complete$Species <- gsub(Odo_complete$Species, pattern = "s_.*$", replacement = "s") 
# remove MD_BC1 which clusters qith WTD in the PCA and must have been mislabelled
Odo_complete <- Odo_complete[!Odo_complete$Genome_ID == "MD_BC1", ]
Odo_complete$Long <- as.numeric(Odo_complete$Long)
Odo_complete$Lat <- as.numeric(Odo_complete$Lat)

# sympatric regions based on NatureServe & IUCN are:
overlap_areas <- c("Arizona", "Colorado", "Idaho", "Kansas", "Montana", "Nebraska", 
                   "North Dakota", "New Mexico", "Oklahoma", "Oregon", "South Dakota", "Utah", 
                   "Wyoming", "Alberta", "Northwest Territories", "Saskatchewan", 
                   "Yukon", "Mexico")
# remove WA & BC from there because overlap only on the east side, not by the coast --> left than -120) remove also Manitoba

# WTD sequenced from sympatric regions
WTD_sympatry <- Odo_complete[Odo_complete$Location1 %in% overlap_areas & 
                               Odo_complete$Sequenced == "Y" &
                               Odo_complete$Species == "O_virginianus", ]
# add east of 120 for WA / BC
tmp <- Odo_complete[Odo_complete$Location1 %in% c("Washington", "British Columbia") &
                      Odo_complete$Long > -120 & 
                      Odo_complete$Species == "O_virginianus" & 
                      Odo_complete$Sequenced == "Y", ]
WTD_sympatry <- rbind(WTD_sympatry, tmp)
# randomly sample 7 individuals
set.seed(1)
WTD_sympatry <- WTD_sympatry[WTD_sympatry$Genome_ID %in% sample(WTD_sympatry$Genome_ID, 7), ]
WTD_sympatry$Group <- "WTD_sympatry"

# MD sequenced from sympatric regions
MD_sympatry <-  Odo_complete[Odo_complete$Location1 %in% overlap_areas & 
                               Odo_complete$Sequenced == "Y" &
                               Odo_complete$Species == "O_hemionus", ]
# add east of 120 for WA / BC
tmp <- Odo_complete[Odo_complete$Location1 %in% c("Washington", "British Columbia") &
                      Odo_complete$Long > -120 & 
                      Odo_complete$Species == "O_hemionus" & 
                      Odo_complete$Sequenced == "Y", ]
MD_sympatry <- rbind(MD_sympatry, tmp)
# randomly sample 7 individuals
set.seed(384)
MD_sympatry <- MD_sympatry[MD_sympatry$Genome_ID %in% sample(MD_sympatry$Genome_ID, size = 7), ]

MD_sympatry$Group <- "MD_sympatry"


# WTD sequenced from allopatric regions, we prefer deer from Canada which were not relocated as opposed to US deers which have a large history of relocations
WTD_allopatry<- Odo_complete[Odo_complete$Sequenced == "Y" &
                               Odo_complete$Species == "O_virginianus" &
                               Odo_complete$Long > -90 &
                               !is.na(Odo_complete$Species), ]
# randomly sample 10 individuals from canadian regions
set.seed(432)
WTD_allopatry <- WTD_allopatry[WTD_allopatry$Genome_ID %in% sample(WTD_allopatry$Genome_ID, size = 7), ]
WTD_allopatry$Group <- "WTD_allopatry"


# MD sequenced from allopatric regions, all 7 individuals
MD_allopatry <- Odo_complete[Odo_complete$Location1 %in% c("Washington", "British Columbia") & 
                               Odo_complete$Long < -120 & 
                               Odo_complete$Sequenced == "Y"| 
                               Odo_complete$Location1 %in% c("Nevada", "California",
                                                             "Alaska") & 
                               Odo_complete$Sequenced == "Y" &
                               Odo_complete$Species == "O_hemionus", ]

MD_allopatry$Group <- "MD_allopatry"



CH01_subset <- rbind(WTD_allopatry, WTD_sympatry, MD_allopatry, MD_sympatry)
CH01_subset <- CH01_subset[, c("Genome_ID", "Species", "Location1", "Sex", "Group", "Sequenced", "Lat", "Long")]

write.table(CH01_subset, file = "../Data/CH01_info.txt", row.names = FALSE, quote = FALSE, sep = "\t")






