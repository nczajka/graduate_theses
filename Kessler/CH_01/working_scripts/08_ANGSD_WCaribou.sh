#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=angsd_ch01
#SBATCH --mem 128G
#SBATCH --cpus-per-task 32
#SBATCH --time=00-72:00

module load StdEnv/2016.4
module load angsd
angsd -bam bam_list_caribou -doMajorMinor 1 -domaf 1 -out ch01_angsd_mapped_caribou -doVcf 1 -nThreads 32 -doGeno 4 -doPost 1 -gl 2 -SNP_pval 1e-6 -minMapQ 20 -minQ 20 -doCounts 1 -skipTriallelic 1 -doGlf 2

