###Aligning source probe sequences from the HorvathMammalMethylChip40 to the moutain goat genome and annotations to only keep probes that align###
#command line and R

#download mountain goat genome from NCBI
wget --recursive -e robots=off --reject "index.html" --no-host-directories --cut-dirs=6 https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA/009/758/055/GCA_009758055.1_ASM975805v1/GCA_009758055.1_ASM975805v1_genomic.fna.gz 
#annotations already in folder

#use HorvathMammal40.Manifest.May2020.manifest.csv to get probe sequences (in Dropbox)
#file too large to send to remote, need to split and reassemble, use -l to split by lines and keep rows together  
split -l 100 Downloads/HorvathMammal40.Manifest.May2020.manifest.csv ./splits/
#send over to remote using scp
#once sent over, put back together
cat * > manifest
#can remove split files now
#check shasum for initial file and file put back together
shasum -a 512 Downloads/HorvathMammal40.Manifest.May2020.manifest.csv 

#trying to make fasta file from manifest (which is a csv file) for probe sequences
#keep IlmnID and SourceSeq columns 
awk -F',' '{print $1,$19}' manifest > manifest.txt
#remove quotes before and after each word/entry
awk -F\" '{$1=$1}1' manifest.txt > manifest_clean.txt
#remove all extra rows that are not CGs, look at data set and remove whatever lines that are not needed
sed '37490,38609d' manifest_clean.txt > probes.txt
#remove first row to remove column names
tail -n +2 probes.txt > probes_try
#merge both IlmnID and SourceSeq columns into one
tr -s ' '  '\n'< probes_try > probes_order
#add > to every second line starting with first to have a fasta format
sed '1~2s/^/>/' probes_order > probes.fa

#want to look like this
#>cg00000165
#AGGATCTGTTAGTACAGTGGCTTTTGATGGAACAGCTGAGGCACACATCG
#>cg00001209
#CTTAACATTATAAGCAGAACAAAGTGGTACAAAATGTATTCAGCCTCACG
#>cg00001364
#CCAGTTTGATATGGATCGCTTATGTGATGAGCACCTGAGAGCAAAGCGCG
#>cg00001582
#GCCCTTTGGAAATAGAATAGCCAATGTAATCTGACACTTCAACTTGCTCG
#>cg00002920
#CATAGTTTATTTTCTTAAAGTATTAATTTGTCAACAGCTTCCCTGCCTCG

#to align probes to genome need ti use nano to create sampleFile that should look like this, (tab between each word):
#FileName	SampleName
#probes.fa	Sample1

#the rest of the code is inspired from: 
#https://figshare.com/articles/online_resource/Genome_Alignment_and_Annotation/13526540?backTo=/collections/Supporting_files_for_Wilkinson_et_al_2021_Genome_Methylation_Predicts_Age_and_Longevity_of_Bats/5257271
# by Amin Haghani to accompany Wilkinson et al. 2021. Genome Methylation Predicts Age and Longevity of Bats. Nature Communications

# The alignment pipeline require the genome fasta file and annotation GFF or GTF files. 
# We aligned the probe source sequences (available in mammalian array manifest file) to different mammalian genomes. 

#the mountain goat genome scaffold and annotation names are different so need to modify them to match
#fix genome to have scaffold names like (ScXtcTc_756;HRSCAF=82837) instead of WJNR01000999.1
#remove start of header (probably better way to do this than what I did) 
#each . represents any input for that position after the >
sed 's/^>...............O/>/' < MG_genome.fa > test
sed 's/^>reamnos americanus isolate splAK2016 />/' < test > test_1
#remove end
sed '/^>/s/.\{31\}$//' test_1 > MG_genome_rename.fa
#the headers should only have this below left and not all the other info
#>ScXtcTc_1000;HRSCAF=99577

#script to align genome with the probes.fa, to use R with scheduler make another script and link it here, add Rscript before to run
#qAlign_R.bash
#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --mem=32G
#SBATCH --time=00-06:00 # time (DD-HH:MM)
#SBATCH --mail-type=ALL
#SBATCH --mail-user=mcossette@trentu.ca
module load r/4.1.0
Rscript qAlign_R.R

#install prior to qAlign_R.bash on R
if (!requireNamespace("BiocManager", quietly = TRUE))
  install.packages("BiocManager")
BiocManager::install("QuasR")

#make this in another nano file
#qAlign_R.R
library(QuasR)
sampleFile <- "probe_file"
alignment <- qAlign(sampleFile, genome="MG_genome_rename.fa", bisulfite = "undir", alignmentParameter = "-k 2 --strata --best -v 3")
# the result will be saved in bam format

#look at how many probes were mapped
samtools flagstat probes_2352c4a575ab5.bam
#31655 + 0 mapped (84.44% : N/A)

#alignment done now need to modify the original HorvathMammal40.Manifest.May2020.manifest.csv again
#Only keep necessary before importing in R
#keep IlmnID, SourceSeq and StrandTB
awk -F',' '{print $1,$14,$19}' manifest > manifest_1.txt
#remove quotes
awk -F\" '{$1=$1}1' manifest_1.txt > manifest_clean_1.txt
#remove all extra rows not CGs, look at data set and remove whatever lines
sed '37490,38609d' manifest_clean_1.txt > probes_1.txt

module load r/4.1.0
#open r/4.1.0 and do the rest of the code through R in command line
R

#load packages necessary
BiocManager::install("Rsamtools")
BiocManager::install(“parallel”)
BiocManager::install("ChIPseeker")
library(ChIPseeker)
library(tidyr)
library(dplyr)
library(parallel)
library(Rsamtools)
library(data.table)

#change bam file to data frame 
aln <- BamFile("probes_2352c4a575ab5.bam")
aln <- scanBam(aln)
aln <- as.data.frame(aln[[1]])


# Determination of CG location based on the probe design. The probe is designed by either top or bottom strand. 
#import the modified HorvathMammal40.Manifest.May2020.manifest.csv file
manifest <- read.table("probes_1.txt", header=TRUE, sep = "")

#add a new column “targetCG” based on values in StrandTB. If StrandTB is B targetCG = 49:50 and if T = 1:2
manifest <- mutate(manifest, targetCG = ifelse(StrandTB == "B", "49:50", "1:2"))                       
#make sure targetCG is in df and is according to StrandTB
head(manifest, n=20)

#select only columns needed in manifest then rename IlmnID column to qname then join the manifest df to the aln df using qname then change targetCG to character
aln_1 <- manifest %>% dplyr::select(IlmnID, SourceSeq, targetCG) %>% dplyr::rename(qname = IlmnID) %>% right_join(aln, by="qname")%>% 
  mutate(targetCG = as.character(manifest$targetCG))

#combine a bunch of info together to create CGcount
CGcount <- rbindlist(lapply(1:nrow(aln_1), function(i){
  pattern <- DNAString(as.character(aln_1$SourceSeq[i]))
  subject <- DNAString(aln_1$seq[i])
  matches <- matchPattern(pattern, subject, max.mismatch = 0, algorithm = "naive-inexact")
  locations = paste(start(matches), end(matches), sep=":")
  pattern2 <-reverseComplement(DNAString(as.character(aln_1$SourceSeq[i])))
  matches2 <- matchPattern(pattern2, subject, max.mismatch = 0, algorithm = "naive-inexact")
  locations2 = paste(start(matches2), end(matches2), sep=":")
  hits <- data.frame(qname=aln_1$qname[i],
                     CGcount = length(start(matches))+length(start(matches2)), 
                     forward = paste(locations, collapse = " ; "),
                     reverse = paste(locations2, collapse = " ; "))
}))

aln_1$alignedStand <- ifelse(CGcount$forward!="", "forward", "complementReverse")
aln_1$targetCG <- ifelse(aln_1$alignedStand=="forward", aln_1$targetCG, 
                       ifelse(aln_1$alignedStand=="complementReverse"&aln_1$targetCG=="1:2", "49:50",
                              ifelse(aln_1$alignedStand=="complementReverse"&aln_1$targetCG=="49:50", "1:2",NA)))
aln_1$targetCG <- as.numeric(as.character(factor(aln_1$targetCG, levels = c("1:2", "49:50"), labels = c(0,48))))
aln_2 <- aln_1 %>% filter(!is.na(pos))

#save as csv, these are all the mapped probes before looking at annotations
write.csv(aln_2, "aln_2.csv")

#convert to GRange to be able to combine with annotations 
input <- aln_2 %>% dplyr::select(qname, rname, strand, pos) %>% dplyr::filter(complete.cases(.)) %>%
  mutate(start = pos) %>% mutate(end = pos+49)
input <- input[,c(2,5,6,1, 3)]
names(input) <- c("chr","start", "end", "CGid", "strand")
target <- with(input,
               GRanges( seqnames = Rle(chr),
                        ranges   = IRanges(start, end=end, names=CGid),
                        strand   = Rle(strand(strand)) ))

#create TxDB with mountain goat annotations file 
library(GenomicFeatures)
txDb <- makeTxDbFromGFF("Oreamnos_americanus_v1_evm.genes.gff3", format = "gff3")


# annotating the probes and estimating the CG location
peakAnno <- annotatePeak(target, tssRegion=c(-10000, 1000),
                         TxDb=txDb,
                         sameStrand = FALSE, overlap = "all", addFlankGeneInfo=T)
genomeAnnotation <- data.frame(CGid = peakAnno@anno@ranges@NAMES, peakAnno@anno, 
                             peakAnno@detailGenomicAnnotation)
genomeAnnotation <- genomeAnnotation %>% dplyr::rename(probeStart = start, probeEnd = end)
genomeAnnotation_1 <- aln_2 %>% dplyr::select(qname, targetCG, seq) %>% 
  dplyr::rename(CGid = qname) %>% 
  right_join(genomeAnnotation, by="CGid") %>% 
  mutate(CGstart = probeStart+targetCG, CGend =probeStart+targetCG+1) %>%
  relocate(... = c(CGstart, CGend, seq), .after = strand) %>% dplyr::select(-targetCG)

#not sure why we name columns ..1 ..2 ..3 but will rename back to what they should be
genomeAnnotation_2 <- genomeAnnotation_1  %>% dplyr::rename(CGstart =...1) %>% dplyr::rename( CGend=...2) %>% dplyr::rename( seq=...3) 

#Confirming if the CG is real. This step is done by extracting the sequence from the original FASTA file, this creates a bed file
BEDfile <- genomeAnnotation_2 %>% dplyr::select(seqnames, CGstart, CGend, 
                                     CGid, strand) %>% 
  setnames(new = c("chrom", 'chromStart', 'chromEnd', 'name', "strand")) %>%
  filter(!is.na(chromStart)) %>% mutate(chromStart = chromStart-1) 
write.table(BEDfile, "BEDfile.bed", 
            sep = "\t", row.names=F, col.names=F, quote = F)

#save bed file to directory so can access it not in R
save(BEDfile.bed, file="BEDfile.bed")

#get out of R, use command line
#use bed file to make fasta
module load bedtools
bedtools getfasta -fi MG_genome_rename.fa -bed BEDfile.bed -fo BEDfile.fasta

#get back into R
#use bed file fasta and create nice data frame with all the necessary information 
CGs <- readDNAStringSet("BEDfile.fasta")
seq_name = names(CGs)
sequence = paste(CGs)
df <- data.frame(seq_name, sequence) %>% dplyr::rename(CG = sequence) %>% 
  mutate(CG = ifelse(CG %in% c("CG", "GC"), TRUE, FALSE))
genomeAnnotation_final <- genomeAnnotation_2 %>% mutate(seq_name = paste(seqnames,":", CGstart-1,"-", CGend, sep = "")) %>% 
  left_join(df) %>% dplyr::select(-seq_name) %>% filter(CG==TRUE) 

#save as csv
write.csv(genomeAnnotation_final, "Anno.csv")
