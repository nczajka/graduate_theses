---
title: "Population structure"
author: C. Kessler
date: "`r format(Sys.time(), '%d %B %Y')`"
output:
  html_document:
    theme: cosmo
    highlight: kate
    toc: true
    toc_depth: 4
    toc_float: true
    code_folding: show
editor_options: 
  chunk_output_type: console
---

# Info

Analyses performed by C.Kessler

# map

```{r message=FALSE, warning=FALSE}
library(ggplot2)
library(rnaturalearth)

# Read
ch02 <- read.table("data/ch02_info_filtered.txt", sep = "\t", header = TRUE)
# add coverage status
highC <- c("MD_WA1", "MD_NM2", "WTD_Key5", "WTD_ON6")
ch02$coverage <- "low"
ch02[ch02$Genome_ID %in% highC, "coverage"] <- "high"

##### statelines ##### 
# read american statelines    
states <- ne_states(country = "united states of america")
# map layer
states.layer <- geom_polygon(aes(x = long, y = lat, group = group),
                            data = states, fill = "white", 
                            color = "black") 
# mexico
all_states <- map_data("world", region = "mexico") 
overlap_mexico <- geom_polygon(aes(x = long, y = lat, group = group),
                               data = all_states, fill = "white", 
                               color = "black")
# Canada
provinces <- ne_states(country = "canada")
provinces.layer <- geom_polygon(aes(x = long, y = lat, group = group),
                                data = provinces, fill = NA, 
                                color = "black")
# all other country borders
world <- ne_countries(scale = "medium", returnclass = "sf")
#Stagger overlapping points
jitter <- position_jitter(width = 0.5, height = 0.5)


##### species distributions ##### 
# https://gis.stackexchange.com/questions/144356/how-to-read-arcgis-files-in-r
library(rgdal)
library(ggmap)
# Read shapefile
WTD_distr <- readOGR("../General_Data/range data/IUCN_WTD/data_0.shp")
MD_distr <- readOGR("../General_Data/range data/IUCN_MD/data_0.shp")
# Convert to lat long
WTD_distr <- spTransform(WTD_distr, CRS("+proj=longlat +datum=WGS84"))
MD_distr <- spTransform(MD_distr, CRS("+proj=longlat +datum=WGS84"))
# Convert shapefile to format ggmap can work with
WTD_distr <- fortify(WTD_distr)
MD_distr <- fortify(MD_distr)
# create polygons
WTD_polygon <- geom_polygon(aes(x = long, y = lat, group = group),
                              data = WTD_distr, fill = "grey" ,
                              alpha = 0.5)
MD_polygon <- geom_polygon(aes(x = long, y = lat, group = group),
               data = MD_distr, fill = "grey",
               alpha = 0.5)


#### plot ####
ggplot(world) + geom_sf(colour = "black", fill = "white") +
  # zoom in
  coord_sf(ylim = c(23.00, 60.00), xlim = c(-140.00, -50.00), expand = FALSE) +
  # US & Canadian layers
  states.layer + provinces.layer + 
  # Mexican & species distribution layers
  overlap_mexico + WTD_polygon + MD_polygon + 
  # samples data
  geom_point(data = ch02, aes(x = Long, y = Lat, colour = pop, shape = coverage), 
             position = jitter, size = 3) +
  scale_colour_manual(values = c("#a2ddbf", "#618573", "#84719b", "#bca2dd"), 
                      breaks = c("MD", "BTD", "Keys", "WTD"),
                      labels = c(parse(text = "MD[MD]"), parse(text = "MD[BTD]"), 
                                 parse(text = "WTD[KEY]"), parse(text = "WTD[ML]"))) +
  scale_shape_manual(values = c(17, 19)) +
  theme_minimal() +
  theme(axis.title = element_blank(), axis.text = element_blank(),
        axis.ticks = element_blank(),
        legend.position = c(0.85, 0.2), legend.title = element_blank(),
        legend.text.align = 0,
        legend.background = element_rect(fill = "white", colour = "grey50"), 
        legend.text = element_text(size = 12)) +
  guides(shape = "none") 

```


# PCAngsd

```{bash echo=T, eval=F}
#### #### #### #### #### #### #### #### #### #### #### ####
# set up general paths
soft=home/ckessler/projects/rrg-shaferab/ckessler/softwares
pcangsd=$soft/software_pcangsd/pcangsd.py # Version 1.02
data=/home/ckessler/projects/rrg-shaferab/ckessler/DATA_ANGSD
# load modules 
module load python/3.10.2

#### #### #### #### #### #### #### #### #### #### #### ####
# All deer individuals
input=$data/deer_filtered.beagle.gz
output=ch02_PCAngsd_allInd_filter
nthreads=13
sbatch -o PCAngsd-%A.out --account=rrg-shaferab --cpus-per-task 8 --mem 208G --account=rrg-shaferab --job-name=PCAngsd --time=0-11:00 --wrap="python $pcangsd -beagle $input -threads $nthreads -o $output" 

# WTD
input=$data/WTD_filtered_angsd.beagle.gz
output=ch02_PCAngsd_WTD_filter
nthreads=4
sbatch -o PCAngsd_WTD-%A.out --account=rrg-shaferab --cpus-per-task 4 --mem 112G --account=rrg-shaferab --job-name=PCAngsd_WTD --time=0-11:00 --wrap="python $pcangsd -beagle $input -threads $nthreads -o $output" 

# MD 
input=$data/MD_filtered_angsd.beagle.gz
output=ch02_PCAngsd_MD_filter
nthreads=12
sbatch -o PCAngsd_MD-%A.out --account=rrg-shaferab --cpus-per-task 6 --mem 24G --account=rrg-shaferab --job-name=PCAngsd_MD --time=0-11:00 --wrap="python $pcangsd -beagle $input -threads $nthreads -o $output" 


```

```{r echo=TRUE, message=FALSE, warning=FALSE}
# plot PCAs
PC_files <- list.files(path = "results/pcangsd", 
                       pattern = "filter", full.names = TRUE)
# add colour
ch02$col <- ch02$pop
ch02[ch02$col == "MD", "col"] <- "#a2ddbf"
ch02[ch02$col == "BTD", "col"] <- "#618573"
ch02[ch02$col == "Keys", "col"] <- "#84719b"
ch02[ch02$col == "WTD", "col"] <- "#bca2dd"


library(ggrepel)
PC_list <- list()
p <- list()
for (f in PC_files) {
  # get dataset name 
  deer_data <- gsub("^.*_PCAngsd_|.cov|_filter", "", f)
  # read table
  PC_list[[deer_data]] <- as.matrix(read.table(f))
  # compute eigenvalues
  eig_mat <- eigen(PC_list[[deer_data]]) 
  # Keep PC1 & 2 in a df
  clustering <- as.data.frame(eig_mat$vectors[, 1:2]) 
  # add specific samples info, colour and label
  if (deer_data == "WTD") {
    info <- ch02[ch02$Species == "O_virginianus", ]
    plabel <- geom_label_repel(aes(label = ifelse(V1 > 0.4 | V1 < -0.2,
                                                  Genome_ID,'')), 
                                   point.padding = 0.05, segment.color = 'grey50',
                               max.overlaps = 100, colour = info$col, 
                               size = 5)  
    
  } else if (deer_data == "MD") {
    info <- ch02[ch02$Species == "O_hemionus", ]
    plabel <- geom_label_repel(aes(label = ifelse(V1 < 0.1, Genome_ID,'')), 
                               point.padding = 0.05, segment.color = 'grey50',
                               max.overlaps = 100, colour = info$col, 
                               size = 5)  
    
  } else if (deer_data == "allInd") {
    info <- ch02
    plabel <- geom_label_repel(aes(label = ifelse(V2 < -0.4 | V1 > 0 & V1 < 0.05,
                                                  Genome_ID,'')), 
                               point.padding = 0.05, segment.color = 'grey50',
                               max.overlaps = 100, colour = info$col, 
                               size = 5)  
    
  } 
  # add info
  clustering <- cbind(clustering, info)
  # compute % variation
  PC1_var <- round(eig_mat$values[1]/sum(eig_mat$values)*100)
  PC2_var <- round(eig_mat$values[2]/sum(eig_mat$values)*100)
  p[[deer_data]] <- ggplot(clustering, aes(x = V1, y = V2, colour = pop)) + 
    geom_point(colour = info$col, size = 3) +
    theme_minimal() +
    theme(axis.text = element_text(size = 12), 
          axis.title = element_text(size = 20)) +
    xlab(paste0("PC1 (", PC1_var, "%)")) +
    ylab(paste0("PC2 (", PC2_var, "%)")) +
    plabel
  print(p[[deer_data]])

  ## correlations
  clustering$Lat <- as.numeric(clustering$Lat)
  clustering$Long <- as.numeric(clustering$Long)
  print(deer_data)
  print(cor.test(clustering$V1, clustering$Lat))
  print(cor.test(clustering$V1, clustering$Long))
  print(cor.test(clustering$V2, clustering$Lat))
  print(cor.test(clustering$V2, clustering$Long))

}

```

As the PCA on MD individuals does not allow for a clear population assignment, we decided to carry out a NGSadmix analysis on those samples.


# NGSadmix on MD

NGSAdmix on MD was run on K = 1 up to 10 with 10 replicates each.

```{bash eval=FALSE}
##### ####### ##### ####### ##### ####### ##### ####### ##### ####### 
# set up variables
NGSadmix=$soft/s_NGSadmix/NGSadmix # version 32
adir=/home/ckessler/scratch/ch02/ngsadmix/md
input=$data/MD_filtered_angsd.beagle.gz
nthreads=8
# load modules
module load angsd/0.939

##### ####### ##### ####### ##### ####### ##### ####### ##### ####### 
# run ngsadmix 10 times for each k 
for j in {1..10} # replicate
do
  
for i in {1..10} # K
do
sbatch -o slurms/NGSadmix_MD_K${i}.${j}-%A.out --account=rrg-shaferab --cpus-per-task 8 --mem 128G --account=rrg-shaferab --job-name=NGSadmix_K${i} --time=0-23:30 --wrap="$NGSadmix -likes $input -K ${i} -o $adir/ch02_NGSadmix_MD_K${i}.${j} -P $nthreads"
sleep 10
done

done

##### ####### ##### ####### ##### ####### ##### ####### ##### ####### 
# Best K
#create logfile
for log in $(ls $adir/*.log) 
do 
grep -Po 'like=\K[^ ]+' $log 
done > logfile

# add k number 
python -c 'print("1\n" * 10, "10\n" * 10, "2\n" * 10, "3\n" * 10, "4\n" * 10, "5\n" * 10, "6\n" * 10, "7\n" * 10, "8\n" * 10, "9\n" * 10, sep = "")' > logfile_knum
paste -d' ' logfile_knum logfile > logfile_formatted

# upload on clumpak
```


Clumpak's results suggest the best K = 2.


```{r message=FALSE, warning=FALSE, fig.height=4}
##### ####### ##### ####### ##### ####### ##### ####### ##### #######
# data preparation
# import data
ngsfiles <- list.files(path = "results/ngsadmix",
                              pattern = ".qopt", full.names = TRUE)

# subset MD individuals
ch02_info_MD <- ch02[ch02$Species == "O_hemionus", c("Genome_ID", "Species")]
library(reshape2)
list_ngs <- list()
p_ngs <- list()
for (f in ngsfiles) {
  # read table
  filename <- gsub("^.*_MD_|.1.qopt", "", f)
  df <- read.table(f)
  df <- cbind(ch02_info_MD, df)
  df$Genome_ID <- reorder(df$Genome_ID, df$V1)
  list_ngs[[filename]] <- df
  # melt for ggplot
  df <- melt(df, measure.vars = grep("V", colnames(df), value = TRUE))
  #plot
  p_ngs[[filename]] <- ggplot(df, aes(x = Genome_ID, y = value, 
                                      fill = variable)) +
    geom_bar(stat = "identity") +
    theme_minimal() +
    theme(axis.text.x = element_text(angle = 45, hjust = 1),
          axis.text = element_text(size = 12),
          axis.title = element_text(size = 18)) +
    guides(fill = "none") +
    labs(y = "Assignment proportion", x = "Sample ID", 
         title = paste("Mule deer population structure with", filename)) + 
    scale_y_continuous(expand = c(0, 0))
  print(p_ngs[[filename]])
  
}

# best K = 2 --> extract samples names for each
list_ngs[["K2"]][list_ngs[["K2"]]$V1 > 0.5, ]
list_ngs[["K2"]][list_ngs[["K2"]]$V1 < 0.5, ]

```

# Figure S1
```{r fig.height=8}
library(patchwork)

(p$WTD | p$MD) / (p_ngs$K2 + plot_spacer()) +
  plot_annotation(tag_levels = "A") + plot_layout(heights = c(2, 1))

```


# Rpackages

```{r eval=TRUE}
sessionInfo()
```





