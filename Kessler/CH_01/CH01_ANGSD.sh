# ANGSD script
# SNP calling of chapter 01, n = 28 deer samples
# Includes vcf file formatting for vctools compatibility and renaming of the individuals

###############################################################################
# set up directory
root=/home/ckessler/projects/rrg-shaferab/ckessler
ch01=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01
analysisdir=/home/ckessler/projects/rrg-shaferab/ckessler/chapter01/data
cd $analysisdir/

###############################################################################
# ccreate suppporting files
# create sample info file CH01_info.txt with nano, initially produced in R: CH01_sampling.R 
# create the bam list with paths to the bam files
# first list all sample files
ls $root/mapping_B*/*realigned.bam > tmp_bam_list
# then, grep to keep only the CH01 samples. For the merged samples, we only keep the downsampled files (BTD_WA1, Odo_ON6, MD_NM2)
{
cat CH01_info.txt | awk  -F"\t" '{print $1}' | # cat the ID column
xargs -i grep {} tmp_bam_list | # grep the ID column into the bam list
# for the merged samples:
grep -Ev 'BTD_WA1_S|MD_NM2_S|Odo_ON6_S' | # remove the first sequencing round
grep -Ev '_merged.realigned.bam' > bam_list # remove the complete merged files
}
cat bam_list | wc # must be 28
rm tmp_bam_list

###############################################################################
# launch ANGSD with onlx variant sites
cp $root/08_ANGSD.sh .
nano 08_ANGSD.sh # change mem to 128GB & cpus to 32, saved as 08_ANGSD_ch01.sh
sbatch -o out_slurms/ANGSD.out 08_ANGSD_ch01.sh

###############################################################################
# launch ANGSD with all sites (remove -snp-pval)
module load StdEnv/2016.4
module load angsd

sbatch --account=rrg-shaferab --job-name=angsd_ch01 --mem 24G --cpus-per-task 6 --time=00-23:00 -o out_slurms/ANGSD_invariants-%A.out --wrap="angsd -bam bam_list -out ch01_deer_invariants -nThreads 6 -doGeno 4 -doMajorMinor 1 -domaf 1 -doPost 1 -gl 2 -minMapQ 20 -minQ 20 -doCounts 1 -skipTriallelic 1"


###############################################################################
#Editing angsd vcf to be compatible with further steps and vcftools 
#We change from ##fileformat=VCFv4.2(angsd version) to ##fileformat=VCFv4.2
module load StdEnv/2018.3 
gunzip -c $analysisdir/deer_angsd.vcf.gz  | head -n 1 # check header

# vcf with the header we want
echo "##fileformat=VCFv4.2" > header.vcf
# remove 1st line from our vcf
sbatch -o out_slurms/format1.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=zip1 --wrap="zcat ch01_deer_angsd.vcf.gz | sed 1d  > temp.vcf"
# insert the new line stored in header.vcf
sbatch -o out_slurms/format2.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=zip2 --wrap="cat header.vcf temp.vcf > ch01_deer_angsd_compatible.vcf"
# check
head -n 1 deer_angsd_compatible.vcf
rm header.vcf temp.vcf

# rename individuals in VCF (from IND01 to Odo_,..), 
# individuals in vcf follow the bam_list order
module load StdEnv/2018.3 
module load bcftools
cd $analysisdir
# create an ordered list of samples IDs
cat bam_list | awk -F/ '$0=$NF' | sed 's/.realigned.bam//' | sed 's/_S[[:digit:]]\+_L00.//' > CH01_samples_list.txt 

# rehaeder in bcftools
sbatch -o out_slurms/indv_naming.out --cpus-per-task 1 --mem 4G --account=rrg-shaferab --time=15:00:00 --job-name=indv_naming --wrap="bcftools reheader -s CH01_samples_list.txt ch01_deer_angsd_compatible.vcf -o ch01_deer_angsd_compatible_named.vcf"

# check that it worked
bcftools query -l ch01_deer_angsd_compatible_named.vcf
cat ch01_deer_angsd_compatible_named.vcf  | head -n 10 | cut -f 1-10 | column -t 

########## ########## ########## ########## ########## ########## ########## 
# reheader .geno file 
gunzip -c ch01_deer_angsd.geno.gz | head -n 5 | column -t # no header

# header shoul be #CHROM POS samplesIDs all tab separated
header1="#CHROM POS"
header2=$(cat pops.txt | cut -f 1 | paste -s)
echo -e $header1"\t"$header2 | sed 's/ \+/\t/g' | gzip > header.gz # gzip for the next step


sbatch -o reheader-%A.out --cpus-per-task 1 --mem=4G --account=rrg-shaferab --time=04:00:00 --job-name=reheader --wrap="zcat header.gz $datadir/ch01_deer_angsd.geno.gz | gzip > ch01_deer_angsd_named.geno.gz"

rm header.gz 

