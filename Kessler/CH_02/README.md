![](Kessler/Images/deer_ppt_template.png)

# Genomic analyses capture the human-induced demographic collapse and recovery in a wide-ranging cervid

Code repository for the publication _Genomic analyses capture the human-induced demographic collapse and recovery in a wide-ranging cervid_, available on BioRxiv

## Table of contents

- [Abstract](## Abstract)

- [Repository description](## Repository description)
  

## Abstract
[(Back to top)](#table-of-contents)

The glacial cycles of the Quaternary heavily impacted species through successions of population contractions and expansions. Similarly, population demography has been intensely shaped by human pressures such as unregulated hunting and land use changes. White-tailed and mule deer survived in different refugia through the Last Glacial Maximum, and their populations were severely reduced after the European colonisation. Here, we analysed 73 re-sequenced deer genomes form across their North American range to understand the consequences of climatic and anthropogenic pressures on deer demographic and adaptive history. We found a strong signal of glacial-induced vicariance and demographic decline; notably, there is a severe decline in white-tailed deer effective population size (Ne) at the end of the Last Glacial Maximum. We found robust evidence for a colonial impact in the form of a recent and dramatic drop in Ne in all analysed populations. Historical census size and restocking data show a clear parallel to historical Ne estimates, but temporal Ne/Nc ratio shows patterns of conservation concern for mule deer. Signatures of selection highlight genes related to temperature, including a cold receptor previously highlighted in woolly mammoth. We also detected numerous immune-genes that we surmise reflect the changing land-use patterns in North America. Our study provides a detailed picture of anthropogenic and climatic-induced decline in deer diversity, and clues to understanding the conservation concerns of mule deer and the successful demographic recovery of white-tailed deer

## Repository description

[(Back to top)](#table-of-contents)

This repository was made for reproducibility purposes, the full script for each analysis is provided in an .Rmd file and corresponding html.

Description of the scripts in order of execution:

- 01_mapping.sh: mapping pipeline from raw fastq files to SNP calling in ANGSD

- 02_PopulationStructure: Samples map, PCAngsd & NGSAdmix analyses

- 03_SummaryStats: Analyses of $F_{ST}$, $pi$, Tajima's D, FROH and iHS

- 04_GONE_MSMC: GONE complete analysis and MSMC plotting only. Contact ABA Shafer for information on the MSMC analysis.

- 05_dadi: Three population dadi analyses

- 06_RDA: Redundancy analysis

