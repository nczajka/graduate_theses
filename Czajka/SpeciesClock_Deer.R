#White-Tailed Deer (Odocoileus virginianus) methylation clock
#Main author: Joseph A. Zoller (from Steve Horvath's lab) 

#installing dependencies
install.packages("ggfortify")
install.packages("shape")
install.packages("RcppEigen")
install.packages("glmnet")
BiocManager::install("impute") 
BiocManager::install("GO.db")
install.packages("WGCNA") 
install.packages("survival")
install.packages("tab")
install.packages("grid")
install.packages("gridExtra")
install.packages("rmcorr")

#loading packages
options(stringAsFactors=F)
library(tidyverse)
library(ggplot2)
library(ggfortify)
library(glmnet)
library(WGCNA) #transposeBigData
library(survival)
library(tab) #tabcoxph
library(grid) #gtable
library(gridExtra) #grid.arrange
library(rmcorr)

##Setting project directories
setwd("~/Dropbox/Methylation_ALLIANCE/Natalie_Czajka_MSc")
#run R_functions_JZ.R before running the rest of this script
source("~/Dropbox/Methylation_ALLIANCE/Natalie_Czajka_MSc/Chapter1/EpigeneticClocks/Scripts/R_functions_JZ.R")

################# Part 1: Loading and Setting Data

#Creating Data Sets
datDeerSamp_tp.rdata=c('Chapter1/ProbeNormalization/Normalized/Deer_AllProbes_normalized_minfi.Rdata')
probes_DEER <- read_csv("CHapter1/ProbeNormalization/Probes/probes_DEER.csv") #species specific

#Loading sample sheet data
infoN15=read.csv('Chapter1/SampleSheetAll_CH1.csv') #is this actually necessary?
infoAllSamp=read.csv('Chapter1/SampleSheetAll_CH1.csv', as.is=T)
infoAllSamp <- infoAllSamp %>%
  dplyr::select(Basename,SpeciesLatinName,OriginalOrderInBatch,Age,ConfidenceInAgeEstimate,
                CanBeUsedForAgingStudies,Tissue,Female,SpeciesCommonName,ExternalSampleID,Folder)
if ("Female" %in% colnames(infoAllSamp)) {
  infoAllSamp$Female[which(is.na(infoAllSamp$Female))] <- "NA"
  infoAllSamp$Female <- factor(infoAllSamp$Female, levels=c(0,1,"NA"))
  levels(infoAllSamp$Female) <- c("Male","Female","NA")
}

#Loading reformatted DNA methylation data
load(datDeerSamp_tp.rdata)
datDeerSamp_tp=normalized_betas_minfi_deer
rm(datDeerSamp_tp.rdata, normalized_betas_minfi_deer)
datDeerSamp <- datDeerSamp_tp %>% dplyr::select(-1) %>% t() %>% as.data.frame()
colnames(datDeerSamp)=datDeerSamp_tp$CGid
datDeerSamp <- cbind(Basename=rownames(datDeerSamp), datDeerSamp)
rownames(datDeerSamp)=seq_len(nrow(datDeerSamp))
datDeerSamp$Basename <- as.character(datDeerSamp$Basename)
rm(datDeerSamp_tp)


################# Part 2: Refining Data

infoCBUAllSamp <- infoAllSamp %>% dplyr::filter(CanBeUsedForAgingStudies=="yes") %>%
  dplyr::filter(ConfidenceInAgeEstimate>=90) %>%
  dplyr::filter(!is.na(Age)) %>%
  dplyr::filter(Basename %in% datDeerSamp$Basename)

### Sample filtering and partitioning
infoAllDeer <- infoAllSamp %>%
  dplyr::filter(SpeciesLatinName %in% c("Odocoileus virginianus"))
#basically rename the whole dataset, only keeping the one that are good to use (CBU = can be used samples, removed outliers)
infoCBUDeer <- infoCBUAllSamp %>%
  dplyr::filter(SpeciesLatinName %in% c("Odocoileus virginianus"))

#making latin, common name and tissue columns factors
infoCBUDeer$SpeciesLatinName <- factor(infoCBUDeer$SpeciesLatinName)
infoCBUDeer$SpeciesCommonName <- factor(infoCBUDeer$SpeciesCommonName)
infoCBUDeer$Tissue <- factor(infoCBUDeer$Tissue)

#selecting samples that can not be used (NBU)
infoNBUDeer <- infoAllDeer %>% dplyr::filter(!Basename %in% infoCBUDeer$Basename) %>% 
dplyr::filter(CanBeUsedForAgingStudies == "yes")

#change columns to factor
infoNBUDeer$SpeciesLatinName <- factor(infoNBUDeer$SpeciesLatinName)
infoNBUDeer$SpeciesCommonName <- factor(infoNBUDeer$SpeciesCommonName)
infoNBUDeer$Tissue <- factor(infoNBUDeer$Tissue)

### Data refinement of CpG Sites, based on shared probe mappings
#remove all columns that don't have matching cgID as column name
datAllSamp_subCPGDeer <- datDeerSamp[,c(1,which(colnames(datDeerSamp) %in% probes_DEER$probeID))]

#latin name of WT deer
table(as.character(infoCBUDeer$Tissue))
latin2common_deer <- unique(dplyr::select(infoCBUDeer,SpeciesLatinName,SpeciesCommonName))

#####USING ALIGNED PROBE DATA####
###Clock pre-analysis, using subsetted CpGs without outlier samples
#setting up training groups for analysis
set.seed(1236)
yxs.train.list <- alignInfoDat(infoCBUDeer,datAllSamp_subCPGDeer,"Basename","Basename")
yxs.test.list <- alignInfoDat(infoNBUDeer,datAllSamp_subCPGDeer,"Basename","Basename")
ys.train <- yxs.train.list[[1]]
xs.train <- yxs.train.list[[2]]
ys.test <- yxs.test.list[[1]]
xs.test <- yxs.test.list[[2]]
rm(yxs.train.list,yxs.test.list)

###Clock analysis, using subsetted CpGs without outlier samples , Training from All
#Outputs are "based on all", not sure what that means
OUTVAR="Age"
out.csv='Deer_Clock_EpigeneticAge.csv'
output.csv='Deer_Clock_EpigeneticAge_PredictedValues.csv'
out.png='Deer_Clock_EpigeneticAge.png'
out.png.title='Deer_Clock_EpigeneticAge'
PREDVAR="DNAmAge"
RESVAR="AgeAccel"
RESinTestVAR="AgeAccelinTest"
ALPHA=0.5
NFOLD=10
#run analysis
ys.output <- saveNetTrained(xs.train,ys.train,xs.test,ys.test,OUTVAR,out.csv,output.csv,out.png,out.png.title,PREDVAR,RESVAR,RESinTestVAR,ALPHA,NFOLD)
#add output data to infoAllDeer dataset 
ys.output <- base::merge(infoAllDeer, ys.output[, c("Basename", "train", PREDVAR, RESVAR, RESinTestVAR)],
                         "Basename", all=T, sort=F)
ys.output <- base::merge(infoAllDeer, ys.output[, c("Basename", "train", PREDVAR, RESVAR, RESinTestVAR)],
                         "Basename", all=T, sort=F)
write.table(ys.output,output.csv,sep=',',row.names=F,quote=F)

## Square-Root Transformed
#Based on all
OUTVAR="Age"
out.csv='Deer_Clock_EpigeneticSqrtAge.csv'
output.csv='Deer_Clock_EpigeneticSqrtAge_PredictedValues.csv'
out.png='Deer_Clock_EpigeneticSqrtAge.png'
out.png.title='Deer_Clock_EpigeneticSqrtAge'
PREDVAR="DNAmAge"
RESVAR="AgeAccel"
RESinTestVAR="AgeAccelinTest"
ALPHA=0.5
NFOLD=10
ys.output <- saveNetTrained(xs.train,ys.train,xs.test,ys.test,OUTVAR,out.csv,output.csv,out.png,out.png.title,PREDVAR,RESVAR,RESinTestVAR,ALPHA,NFOLD,fun_trans=fun_sqrt_trans,fun_inv=fun_sqrt_inv)
ys.output <- base::merge(infoAllDeer, ys.output[, c("Basename", "train", PREDVAR, RESVAR, RESinTestVAR)],
                         "Basename", all=T, sort=F)
ys.output <- base::merge(infoAllDeer, ys.output[, c("Basename", "train", PREDVAR, RESVAR, RESinTestVAR)],
                         "Basename", all=T, sort=F)
write.table(ys.output,output.csv,sep=',',row.names=F,quote=F)

## Logarithm Transformed
OUTVAR="Age"
out.csv='Deer_Clock_EpigeneticLogAge.csv'
output.csv='Deer_Clock_EpigeneticLogAge_PredictedValues.csv'
out.png='Deer_Clock_EpigeneticLogAge.png'
out.png.title='Deer_Clock_EpigeneticLogAge'
PREDVAR="DNAmAge"
RESVAR="AgeAccel"
RESinTestVAR="AgeAccelinTest"
ALPHA=0.5
NFOLD=10
ys.output <- saveNetTrained(xs.train,ys.train,xs.test,ys.test,OUTVAR,out.csv,output.csv,out.png,out.png.title,PREDVAR,RESVAR,RESinTestVAR,ALPHA,NFOLD,fun_trans=fun_log_trans,fun_inv=fun_log_inv)
ys.output <- base::merge(infoAllDeer, ys.output[, c("Basename", "train", PREDVAR, RESVAR, RESinTestVAR)],
                         "Basename", all=T, sort=F)
ys.output <- base::merge(infoAllDeer, ys.output[, c("Basename", "train", PREDVAR, RESVAR, RESinTestVAR)],
                         "Basename", all=T, sort=F)
write.table(ys.output,output.csv,sep=',',row.names=F,quote=F)
