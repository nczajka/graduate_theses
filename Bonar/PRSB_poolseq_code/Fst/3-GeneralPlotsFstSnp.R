### fst Plots for Poolseq ====
# Authors: Maegwin Bonar
# Purpose: generate full manhattan plots for all outlier SNPs
# Inputs: igv file of the fst calculation generated using popoolation2
# Outputs: manhattan plots
# License: /LICENSE.md 
# Title: 

### Packages ----
libs <- c('ggplot2','dplyr','data.table','plyr', 'ggpubr', 'stringr')
lapply(libs, require, character.only = TRUE)

### Input raw data ----
# Igv format file

fst50 <- read.table('input/Fst_top50_fst.igv', header = T)
setDT(fst50)[, paste0("CHR", 1:3) := tstrsplit(Chromosome, "_")]

fst67 <- read.table('input/Fst_top67_fst.igv', header = T)
setDT(fst67)[, paste0("CHR", 1:3) := tstrsplit(Chromosome, "_")]

fet50 <- read.table('input/Fet_top50_fst.igv', header = T)
setDT(fet50)[, paste0("CHR", 1:3) := tstrsplit(Chromosome, "_")]

fet67 <- read.table('input/Fet_top67_fst.igv', header = T)
setDT(fet67)[, paste0("CHR", 1:3) := tstrsplit(Chromosome, "_")]


#Chromosome alignment
mummer <- read.table('input/wtd_cow_mummer_sorted_filtered.dms')
names(mummer) <- c("1", "2", "3", "4", "COW", "WTD")
mummer <- mummer[,c("COW","WTD")]

CHR_labels <- read.table("input/CHR_labels.txt")
names(CHR_labels) <- c("CHR","COW")

mummer_labels <- join(mummer, CHR_labels, by='COW')
mummer_labels$CHR[is.na(mummer_labels$CHR)] <- 32
mummer_labels<- mummer_labels[,c('WTD', 'CHR')]
names(mummer_labels) <- c("CHR1", "CHR")

# join chromosome labels to fst file
fst50_labels <- join(fst50, mummer_labels, by='CHR1')
fst67_labels <- join(fst67, mummer_labels, by='CHR1')
fet50_labels <- join(fet50, mummer_labels, by='CHR1')
fet67_labels <- join(fet67, mummer_labels, by='CHR1')

# To make a manhattan plot in ggplot you need to get the cumulative position of 
# all the SNPs so that they all plot in order. I followed this tutorial to do 
# this but did it using data.table instead of dplyr since I don't know how to use that.
# https://www.r-graph-gallery.com/101_Manhattan_plot.html

dtlist <- list(fst50_labels, fst67_labels, fet50_labels, fet67_labels)
filenames <- list('Fst50', 'Fst67', 'Fet50', 'Fet67')

#### individual plots per comparison ----
for (i in 1:length(dtlist)) {
  dt <- dtlist[[i]]
  dt<-fst50_labels
  # this finds us the length of each chromosome
  d <-dt[, max(End), by = c('CHR', 'CHR1')]
  # I ordered the datatable for some reason the chromosomes weren't in order
  d <-d[order(CHR)]
  # then you greate a total cumulative position of each chromosome
  d[, tot := cumsum(V1)-V1]
  # keep only the variables you need
  d <- d[,c('CHR1', 'tot')]
  
  # Add the cumulative poisition to the initial dataset (I created a new one here)
  dt_gg <- as.data.table(join(dt, d, by='CHR1'))
  # Add cumulative poisition of each SNP
  dt_gg[, BPcum := Start+tot, by = CHR1]
  
  # Here you are generating the center position of each chromosome so you can lable it properly
  axisdf <- dt_gg[, (max(BPcum) + min(BPcum) ) / 2, by = CHR]
  
  ### NM vs NMS ----
  NM_NMS <-ggplot(dt_gg, aes(x=BPcum, y=X1.2)) +
    # Show all points
    geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
    scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
    # custom X axis:
    scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1) +
    scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3, 0.4), limits = c(0,0.4)) +     # remove space between plot area and x axis
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank()
    )+
    xlab("Chromosome")+
    ylab("FST")+
    ggtitle('North Magnolia (North) vs. North Magnolia (South)')
  
  png(paste("output/Fst/Outliers/NM_NMS_FstSnp", filenames[[i]], ".png", sep = ""),width=1200,height=600,units="px",res=150, pointsize = 3)
  print(NM_NMS)
  dev.off()
  
  ### NM vs NR ----
  NM_NR <- ggplot(dt_gg, aes(x=BPcum, y=X1.3)) +
    # Show all points
    geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
    scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
    # custom X axis:
    scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
    scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3, 0.4), limits = c(0,0.4))+     # remove space between plot area and x axis
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank()
    )+
    xlab("Chromosome")+
    ylab("FST")+
    ggtitle('North Magnolia (North) vs. North Ridge')
  
  png(paste("output/Fst/Outliers/NM_NR_FstSnp", filenames[[i]], ".png", sep = ""),width=1200,height=600,units="px",res=150, pointsize = 3)
  print(NM_NR)
  dev.off()
  
  ### NM vs RG ----
  NM_RG <- ggplot(dt_gg, aes(x=BPcum, y=X1.4)) +
    # Show all points
    geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
    scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
    # custom X axis:
    scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
    scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3, 0.4), limits = c(0,0.4)) +     # remove space between plot area and x axis
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank()
    )+
    xlab("Chromosome")+
    ylab("FST")+
    ggtitle('North Magnolia (North) vs. Ryan Gulch')
  
  png(paste("output/Fst/Outliers/NM_RG_FstSnp", filenames[[i]], ".png", sep = ""),width=1200,height=600,units="px",res=150, pointsize = 3)
  print(NM_RG)
  dev.off()
  
  ### NM vs SM ----
  NM_SM <- ggplot(dt_gg, aes(x=BPcum, y=X1.5)) +
    # Show all points
    geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
    scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
    # custom X axis:
    scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
    scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3, 0.4), limits = c(0,0.4)) +     # remove space between plot area and x axis
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank()
    )+
    xlab("Chromosome")+
    ylab("FST")+
    ggtitle('North Magnolia (North) vs. South Magnolia')
  
  png(paste("output/Fst/Outliers/NM_SM_FstSnp", filenames[[i]], ".png", sep = ""),width=1200,height=600,units="px",res=150, pointsize = 3)
  print(NM_SM)
  dev.off()
  
  ### NMS vs NR ----
  NMS_NR <- ggplot(dt_gg, aes(x=BPcum, y=X2.3)) +
    # Show all points
    geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
    scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
    # custom X axis:
    scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
    scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3, 0.4), limits = c(0,0.4)) +     # remove space between plot area and x axis
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank()
    )+
    xlab("Chromosome")+
    ylab("FST")+
    ggtitle('North Magnolia (South) vs. North Ridge')
  
  png(paste("output/Fst/Outliers/NMS_NR_FstSnp", filenames[[i]], ".png", sep = ""),width=1200,height=600,units="px",res=150, pointsize = 3)
  print(NMS_NR)
  dev.off()
  
  ### NMS vs RG ----
  NMS_RG <- ggplot(dt_gg, aes(x=BPcum, y=X2.4)) +
    # Show all points
    geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
    scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
    # custom X axis:
    scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
    scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3, 0.4), limits = c(0,0.4)) +     # remove space between plot area and x axis
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank()
    )+
    xlab("Chromosome")+
    ylab("FST")+
    ggtitle('North Magnolia (South) vs. Ryan Gulch')
  
  png(paste("output/Fst/Outliers/NMS_RG_FstSnp", filenames[[i]], ".png", sep = ""),width=1200,height=600,units="px",res=150, pointsize = 3)
  print(NMS_RG)
  dev.off()
  
  ### NMS vs SM ----
  NMS_SM <- ggplot(dt_gg, aes(x=BPcum, y=X2.5)) +
    # Show all points
    geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
    scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
    # custom X axis:
    scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
    scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3, 0.4), limits = c(0,0.4)) +     # remove space between plot area and x axis
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank()
    )+
    xlab("Chromosome")+
    ylab("FST")+
    ggtitle('North Magnolia (South) vs. South Magnolia')
  
  png(paste("output/Fst/Outliers/NMS_SM_FstSnp", filenames[[i]], ".png", sep = ""),width=1200,height=600,units="px",res=150, pointsize = 3)
  print(NMS_SM)
  dev.off()
  
  ### NR vs RG ----
  NR_RG <- ggplot(dt_gg, aes(x=BPcum, y=X3.4)) +
    # Show all points
    geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
    scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
    # custom X axis:
    scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
    scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3, 0.4), limits = c(0,0.4)) +     # remove space between plot area and x axis
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank()
    )+
    xlab("Chromosome")+
    ylab("FST")+
    ggtitle('North Ridge vs. Ryan Gulch')
  
  png(paste("output/Fst/Outliers/NR_RG_FstSnp", filenames[[i]], ".png", sep = ""),width=1200,height=600,units="px",res=150, pointsize = 3)
  print(NR_RG)
  dev.off()
  
  ### NR vs SM ----
  NR_SM <- ggplot(dt_gg, aes(x=BPcum, y=X3.5)) +
    # Show all points
    geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
    scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
    # custom X axis:
    scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
    scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3, 0.4), limits = c(0,0.4)) +     # remove space between plot area and x axis
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank()
    )+
    xlab("Chromosome")+
    ylab("FST")+
    ggtitle('North Ridge vs. South Magnolia')
  
  png(paste("output/Fst/Outliers/NR_SM_FstSnp", filenames[[i]], ".png", sep = ""),width=1200,height=600,units="px",res=150, pointsize = 3)
  print(NR_SM)
  dev.off()
  
  ### RG vs SM ----
  RG_SM <- ggplot(dt_gg, aes(x=BPcum, y=X4.5)) +
    # Show all points
    geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
    scale_color_manual(values = rep(c("grey", "skyblue"), 32)) +
    # custom X axis:
    scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1 ) +
    scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3, 0.4), limits = c(0,0.4)) +     # remove space between plot area and x axis
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank()
    )+
    xlab("Chromosome")+
    ylab("FST")+
    ggtitle('Ryan Gulch vs. South Magnolia')
  
  png(paste("output/Fst/Outliers/RG_SM_FstSnp", filenames[[i]], ".png", sep = ""), width=1200,height=600,units="px",res=150, pointsize = 3)
  print(RG_SM)
  dev.off()
  
  ### put together in plots ----
  all <- ggarrange(NM_NMS, NM_RG, NM_SM, NMS_NR, NR_RG, NR_SM,
                     ncol = 2, nrow = 3)
  png(paste("output/Fst/Outliers/ComparisonsFstSnp", filenames[[i]], ".png", sep = ""), width=2400,height=1200,units="px",res=150, pointsize = 3)
  print(all)
  dev.off()
  
  allopp <- ggarrange(NM_NR, NMS_RG, NMS_SM, RG_SM,
                      ncol = 2, nrow = 2)
  
  png(paste("output/Fst/Outliers/ComparisonsFstSnp_opp", filenames[[i]], ".png", sep = ""), width=2400,height=1200,units="px",res=150, pointsize = 3)
  print(allopp)
  dev.off()
  
}


### average values plots ---- 
#get the average values across comparisons 'X1.2', 'X1.4', 'X1.5', 'X2.3',
# 'X3.4','X3.5 and same comparisons 'X1.3', 'X2.4','X2.5', 'X4.5'

for (i in 1:length(dtlist)) {
  dt <- dtlist[[i]]
  dt <- fst50_labels
  #make the average columns by averaging across correct rows
  dt[, AvgFst1 := rowMeans(dt[,c(5,7,8,9,12,13)])]
  dt[, AvgFst2 := rowMeans(dt[,c(6,10,11,14)])]
  
  # this finds us the length of each chromosome
  d <-dt[, max(End), by = c('CHR', 'CHR1')]
  # I ordered the datatable for some reason the chromosomes weren't in order
  d <-d[order(CHR)]
  # then you greate a total cumulative position of each chromosome
  d[, tot := cumsum(V1)-V1]
  # keep only the variables you need
  d <- d[,c('CHR1', 'tot')]
  
  # Add the cumulative poisition to the initial dataset (I created a new one here)
  dt_gg <- as.data.table(join(dt, d, by='CHR1'))
  # Add cumulative poisition of each SNP
  dt_gg[, BPcum := Start+tot, by = CHR1]
  
  # Here you are generating the center position of each chromosome so you can lable it properly
  axisdf <- dt_gg[, (max(BPcum) + min(BPcum) ) / 2, by = CHR]
  
  ### plot of AvgFst1 (comparison of different phenotypes)
  Fst1 <-ggplot(dt_gg, aes(x=BPcum, y=AvgFst1)) +
    # Show all points
    geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
    geom_point(data=m, aes(x=BPcum, y=AvgFst1), colour = 'red') +
    scale_color_manual(values = rep(c("grey", "#92C019"), 32)) +
    # custom X axis:
    scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1) +
    scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3), limits = c(0,0.3)) +     # remove space between plot area and x axis
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank()
    )+
    xlab("Chromosome")+
    ylab("FST")
    #ggtitle(paste('Average FST north vs. south migration', filenames[[i]], sep = " - "))
  
  png(paste("output/Fst/Outliers/AvgFst1Snp", filenames[[i]], ".png", sep = ""),width=1200,height=600,units="px",res=150, pointsize = 3)
  print(Fst1)
  dev.off()
  
  Fst2 <-ggplot(dt_gg, aes(x=BPcum, y=AvgFst2)) +
    # Show all points
    geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
    scale_color_manual(values = rep(c("grey", "#92C019"), 32)) +
    # custom X axis:
    scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1) +
    scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3), limits = c(0,0.3)) +     # remove space between plot area and x axis
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank()
    )+
    xlab("Chromosome")+
    ylab("FST")
    #ggtitle(paste('Average FST same migration direction', filenames[[i]], sep = " - "))
  
  png(paste("output/Fst/Outliers/AvgFst2Snp", filenames[[i]], ".png", sep = ""),width=1200,height=600,units="px",res=150, pointsize = 3)
  print(Fst2)
  dev.off()
  
  #plot the Fst comparisons on the same plot
  Fstall <- ggplot(dt_gg, aes(x=BPcum, y=AvgFst1)) +
    # Show all points
    geom_point( colour = "#92C019", alpha=1, size=1.3) +
    #scale_color_manual(values = rep(c("black", "blue"), 32)) +
    geom_point( aes(x = BPcum, y=AvgFst2),colour = 'grey', alpha=0.8, size=1.3) +
    #geom_hline(aes(yintercept = quantile(dt_gg$AvgFst1,prob=1-1/100)), 
               #colour = "red", linetype = 'dashed', alpha = 0.5)+
    #geom_hline(aes(yintercept = quantile(dt_gg$AvgFst1,prob=1-10/100)), 
              #colour = "red", linetype = 'dashed', alpha = 0.5)+
    # custom X axis:
    scale_x_continuous(label = axisdf$CHR, breaks= axisdf$V1) +
    scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3), limits = c(0,0.3)) +     # remove space between plot area and x axis
    # Custom the theme:
    theme_bw() +
    theme( 
      legend.position="none",
      panel.border = element_blank(),
      panel.grid.major.x = element_blank(),
      panel.grid.minor.x = element_blank()
    )+
    xlab("Chromosome")+
    ylab("FST")+
    ggtitle(paste('Average FST across all comparisons', filenames[[i]], sep = " - "))
 
  # put it all together
  all <- ggarrange(Fst1, Fst2, Fstall,
                   ncol = 2, nrow = 2)
  png(paste("output/Fst/Outliers/ComparisonsAvgFstSnp", filenames[[i]], ".png", sep = ""), width=2400,height=1200,units="px",res=150, pointsize = 3)
  print(all)
  dev.off()
  
}


chr1.3 <- dt_gg[CHR==1 | CHR==2 | CHR==3 | CHR==4]
axisdf1.3 <- axisdf[CHR==1 | CHR==2 | CHR==3 | CHR==4]

# chr1.3$ID <- paste(chr1.3$Chromosome, chr1.3$End, sep = "_")
# chrwin <- chr1.3[chr1.3$ID %in% windows,]

Fst1 <-ggplot(chr1.3, aes(x=BPcum, y=AvgFst1)) +
  # Show all points
  geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
  #geom_point(data = chrwin, aes(x=BPcum, y=AvgFst1), colour = 'red')+
  scale_color_manual(values = rep(c("grey", "#92C019"), 32)) +
  # custom X axis:
  scale_x_continuous(label = axisdf1.3$CHR, breaks= axisdf1.3$V1) +
  scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3), limits = c(0,0.3)) +      # remove space between plot area and x axis
  # Custom the theme:
  theme_bw() +
  theme( 
    legend.position="none",
    panel.border = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank()
  )+
  xlab("Chromosome")+
  ylab("FST")+
  ggtitle(paste('Average FST north vs. south migration','Fst', sep = " - "))

Fst2 <-ggplot(chr1.3, aes(x=BPcum, y=AvgFst2)) +
  # Show all points
  geom_point( aes(color=as.factor(CHR)), alpha=0.8, size=1.3) +
  scale_color_manual(values = rep(c("grey", "#92C019"), 32)) +
  # custom X axis:
  scale_x_continuous(label = axisdf1.3$CHR, breaks= axisdf1.3$V1) +
  scale_y_continuous(breaks = c(0, 0.1, 0.2, 0.3), limits = c(0,0.3)) +     # remove space between plot area and x axis
  # Custom the theme:
  theme_bw() +
  theme( 
    legend.position="none",
    panel.border = element_blank(),
    panel.grid.major.x = element_blank(),
    panel.grid.minor.x = element_blank()
  )+
  xlab("Chromosome")+
  ylab("FST")+
  ggtitle(paste('Average FST same migration direction', "Fst", sep = " - "))

all1.3 <- ggarrange(Fst1, Fst2,
                    ncol = 1, nrow = 2)
all1.3

