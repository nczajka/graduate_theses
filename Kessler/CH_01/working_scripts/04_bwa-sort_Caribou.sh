#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bwa-bam-sort
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=0-23:00 # time (DD-HH:MM)

module load StdEnv/2016.4
module load bwa
module load samtools
echo ${1}

bwa mem -M -t 16 -R "@RG\\tID:${1}\\tSM:${1}\\tLB:${1}\\tPL:ILLUMINA" \
${2} ${1}_realigned_sorted_R1.fq ${1}_realigned_sorted_R2.fq | \
samtools sort -@ 16 -o ${1}_realign_caribou.sorted_reads.bam
[ckessler@gra-login2 chapter01]$ 