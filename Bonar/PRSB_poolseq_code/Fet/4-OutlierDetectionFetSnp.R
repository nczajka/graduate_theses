### fet Data prep and outlier SNP detection ====
# Authors: Maegwin Bonar
# Purpose: Prep data and select outlier SNPs
# Inputs: igv file of the fet calculation generated using popoolation2
# Outputs: list of outlier SNPs and coordinates in a .bed file format
# License: /LICENSE.md 
# Title: 

### Packages ----
libs <- c('ggplot2','dplyr','data.table','plyr', 'ggpubr', 'sjmisc', 'stringr')
lapply(libs, require, character.only = TRUE)

### Input raw data ----
# Igv format file

fst50 <- read.table('input/Fst_top50_fet.igv', header = T)
setDT(fst50)[, paste0("CHR", 1:3) := tstrsplit(Chromosome, "_")]

fst67 <- read.table('input/Fst_top67_fet.igv', header = T)
setDT(fst67)[, paste0("CHR", 1:3) := tstrsplit(Chromosome, "_")]

fet50 <- read.table('input/Fet_top50_fet.igv', header = T)
setDT(fet50)[, paste0("CHR", 1:3) := tstrsplit(Chromosome, "_")]

fet67 <- read.table('input/Fet_top67_fet.igv', header = T)
setDT(fet67)[, paste0("CHR", 1:3) := tstrsplit(Chromosome, "_")]

#### loop across all datasets
dtlist <- list(fst50, fst67, fet50, fet67)
filenames <- list('Fst50', 'Fst67', 'Fet50', 'Fet67')

for (i in 1:length(dtlist)) {
  
  fet <- dtlist[[i]]
  #### Separate file with all comparisons into single comparison groups
  X12 <- fet[, c('CHR1','Start', 'End','X1.2')]
  
  X13 <- fet[, c('CHR1','Start', 'End','X1.3')]
  
  X14 <- fet[, c('CHR1','Start', 'End','X1.4')]
  
  X15 <- fet[, c('CHR1', 'Start','End','X1.5')]
  
  X23 <- fet[, c('CHR1','Start', 'End','X2.3')]
  
  X24 <- fet[, c('CHR1','Start', 'End','X2.4')]
  
  X25 <- fet[, c('CHR1', 'Start','End','X2.5')]
  
  X34 <- fet[, c('CHR1', 'Start','End','X3.4')]
  
  X35 <- fet[, c('CHR1', 'Start','End','X3.5')]
  
  X45 <- fet[, c('CHR1','Start', 'End', 'X4.5')]
  
  ### Select the top 1% out of all relevant comparisions (in this case 6 comparisions of different migratory directions) ----
  X12<- X12[X12$X1.2 >  -log10(0.001)]
  X12[, 'ID' := paste(CHR1, Start, End, sep = '_')]
  
  X14<- X14[X14$X1.4 >  -log10(0.001)]
  X14[, 'ID' := paste(CHR1, Start, End, sep = '_')]
  
  X15<- X15[X15$X1.5 >  -log10(0.001)]
  X15[, 'ID' := paste(CHR1, Start, End, sep = '_')]
  
  
  X23<- X23[X23$X2.3 >  -log10(0.001)]
  X23[, 'ID' := paste(CHR1, Start, End, sep = '_')]
  
  
  X34<- X34[X34$X3.4 >  -log10(0.001)]
  X34[, 'ID' := paste(CHR1, Start, End, sep = '_')]
  
  
  X35<- X35[X35$X3.5 >  -log10(0.001)]
  X35[, 'ID' := paste(CHR1, Start, End, sep = '_')]
  
  
  #merge groups together
  top <- full_join(X12, X14, by = 'ID')
  top <- full_join(top, X15, by = 'ID')
  top <- full_join(top, X23, by = 'ID')
  top <- full_join(top, X34, by = 'ID')
  top <- full_join(top, X35, by = 'ID')
  
  # keep only columns needed
  top <- top[, c('ID', 'X1.2', 'X1.4', 'X1.5', 'X2.3', 'X3.4','X3.5')]
  # count the number of NAs across the pools
  top <- row_count(top, X1.2:X3.5, count = NA, append = TRUE)
  # keep only those outliers that show up in 4/6 pools
  top67 <- top[ rowcount <= 2, ]
  top50 <- top[ rowcount <= 3, ]
  
  ### Do the same thing for comparisons of same migratory direction ----
  X13<- X13[X13$X1.3 >  -log10(0.001)]
  X13[, 'ID' := paste(CHR1, Start, End, sep = '_')]
  
  
  X24<- X24[X24$X2.4 >  -log10(0.001)]
  X24[, 'ID' := paste(CHR1, Start, End, sep = '_')]
  
  
  X25<- X25[X25$X2.5 >  -log10(0.001)]
  X25[, 'ID' := paste(CHR1, Start, End, sep = '_')]
  
  
  X45<- X45[X45$X4.5 >  -log10(0.001)]
  X45[, 'ID' := paste(CHR1, Start, End, sep = '_')]
  
  
  #merge groups together
  top_opp <- full_join(X13, X24, by = 'ID')
  top_opp <- full_join(top_opp, X25, by = 'ID')
  top_opp <- full_join(top_opp, X45, by = 'ID')
  setDT(top_opp)
  
  # keep only columns needed
  top_opp <- top_opp[, c('ID', 'X1.3', 'X2.4', 'X2.5', 'X4.5')]
  
  ## outliers in at least 1 comparison
  top_all <- left_join(top, top_opp, by = 'ID')
  top_all<- row_count(top_all, X1.3:X4.5, count = NA, append = TRUE)
  top_all <- top_all[rowcount...13 == 4, ]
  
  # Keep only the ID column and split it back into scaffold, start, end
  topall <- data.table(top_all[, str_split(ID, "_", simplify = TRUE)])
  
  # top SNPs that are in 4/6 comparisions
  top_all67 <- left_join(top67, top_opp, by = 'ID')
  top_all67<- row_count(top_all67, X1.3:X4.5, count = NA, append = TRUE)
  # none of the comparisons of same migration direction
  top_all67 <- top_all67[rowcount...13 == 4, ]
  # Keep only the ID column and split it back into scaffold, start, end
  top_all67 <- data.table(top_all67[, str_split(ID, "_", simplify = TRUE)])

  # top SNPs that are in 3/6 comparisions
  top_all50 <- left_join(top50, top_opp, by = 'ID')
  top_all50<- row_count(top_all50, X1.3:X4.5, count = NA, append = TRUE)
  # none of the comparisons of same migration direction
  top_all50 <- top_all50[rowcount...13 == 4, ]
  # Keep only the ID column and split it back into scaffold, start, end
  top_all50 <- data.table(top_all50[, str_split(ID, "_", simplify = TRUE)])

  write.table(top_all50, paste('output/fetOutlierSnp_all50', filenames[[i]], '.bed', sep = ""), row.names = FALSE, 
              quote = FALSE, sep = "\t",col.names = FALSE)
  write.table(top_all67, paste('output/fetOutlierSnp_all67', filenames[[i]], '.bed', sep = ""), row.names = FALSE, 
              quote = FALSE, sep = "\t",col.names = FALSE)
  write.table(topall, paste('output/fetOutlierSnp_all', filenames[[i]], '.bed', sep = ""), row.names = FALSE, 
              quote = FALSE, sep = "\t",col.names = FALSE)
}
