#Histogram for distribution of delta values in Universal  and species specific Clocks
#Figure 3

setwd("~/Dropbox/Methylation_ALLIANCE/Natalie_Czajka_MSc/Chapter1/EpigeneticClocks")
library(readxl)
library(dplyr)
library(ggplot2)

all_delta <- read_excel("AllSpeciesClocks_FINAL.xlsx") %>%
  dplyr::select(ExternalSampleID,Basename,SpeciesLatinName,delta_UC2, delta_SSC)
bear_delta <- all_delta %>% 
  dplyr::filter(SpeciesLatinName %in% c("Ursus americanus")) 
deer_delta <- all_delta %>% 
  dplyr::filter(SpeciesLatinName %in% c("Odocoileus virginianus"))
mg_delta <- all_delta %>% 
  dplyr::filter(SpeciesLatinName %in% c("Oreamnos americanus")) 

par(mfrow = c(2, 3))

########Delta UC2
####Bear
bear_delta$delta_UC2 <- as.numeric(bear_delta$delta_UC2)

colfunc<-colorRampPalette(c("firebrick", "brown", "brown4","peru", "sienna", "sienna4", "saddlebrown"))
hist(bear_delta$delta_UC2, col=(colfunc(17)), main = "Black bear", xlab="Δ (UC2-CA)", breaks=seq(-7,10,1), xaxp=c(-7,10,17), ylim=c(0,15), yaxp=c(0,15,15))
mtext("A", font=2, side=3, line=1, adj=-0.15, cex = 1)
abline(h=seq(0,15,1), col="gray", lty="dotted")
abline(v=0, col="red", lwd=3, lty=2)

####Deer
deer_delta$delta_UC2 <- as.numeric(deer_delta$delta_UC2)

colfunc<-colorRampPalette(c("chartreuse", "yellowgreen", "forestgreen", "darkgreen", "palegreen4", "mediumspringgreen", "seagreen"))
hist(deer_delta$delta_UC2, col=(colfunc(17)), main = "White-tailed deer", xlab="Δ (UC2-CA)", breaks=seq(-7,6,1), xaxp=c(-7,6,13), ylim=c(0,16), yaxp=c(0,16,16))
mtext("B", font=2, side=3, line=1, adj=-0.15, cex = 1)
abline(h=seq(0,15,1), col="gray", lty="dotted")
abline(v=0, col="red", lwd=3, lty=2)

####Mountain Goat
mg_delta$delta_UC2 <- as.numeric(mg_delta$delta_UC2)

colfunc<-colorRampPalette(c("aliceblue", "paleturquoise2", "skyblue", "royalblue", "mediumslateblue", "darkblue", "navy", "navyblue"))
hist(mg_delta$delta_UC2, col=(colfunc(17)), main = "Mountain goat", xlab="Δ (UC2-CA)", breaks=seq(-12,5,1), xaxp=c(-12,5,17), ylim=c(0,15), yaxp=c(0,15,15))
mtext("C", font=2, side=3, line=1, adj=-0.15, cex = 1)
abline(h=seq(0,15,1), col="gray", lty="dotted")
abline(v=0, col="red", lwd=3, lty=2)

########Delta SSC
####Bear
bear_delta$delta_SSC <- as.numeric(bear_delta$delta_SSC)

colfunc<-colorRampPalette(c("firebrick", "brown", "brown4","peru", "sienna", "sienna4", "saddlebrown"))
hist(bear_delta$delta_SSC, col=(colfunc(17)), main = "Black bear", xlab="Δ (SSC-CA)", breaks=seq(-7,10,1), xaxp=c(-7,10,17), ylim=c(0,15), yaxp=c(0,15,15))
mtext("D", font=2, side=3, line=1, adj=-0.15, cex = 1)
abline(h=seq(0,15,1), col="gray", lty="dotted")
abline(v=0, col="red", lwd=3, lty=2)

####Deer
deer_delta$delta_SSC <- as.numeric(deer_delta$delta_SSC)

colfunc<-colorRampPalette(c("chartreuse", "yellowgreen", "forestgreen", "darkgreen", "palegreen4", "mediumspringgreen", "seagreen"))
hist(deer_delta$delta_SSC, col=(colfunc(17)), main = "White-tailed deer", xlab="Δ (SSC-CA)", breaks=seq(-7,6,1), xaxp=c(-7,6,13), ylim=c(0,15), yaxp=c(0,15,15))
mtext("E", font=2, side=3, line=1, adj=-0.15, cex = 1)
abline(h=seq(0,15,1), col="gray", lty="dotted")
abline(v=0, col="red", lwd=3, lty=2)

####Mountain Goat
mg_delta$delta_SSC <- as.numeric(mg_delta$delta_SSC)

colfunc<-colorRampPalette(c("aliceblue", "paleturquoise2", "skyblue", "royalblue", "mediumslateblue", "darkblue", "navy", "navyblue"))
hist(mg_delta$delta_SSC, col=(colfunc(17)), main = "Mountain goat", xlab="Δ (SSC-CA)", breaks=seq(-12,5,1), xaxp=c(-12,5,17), ylim=c(0,15), yaxp=c(0,15,15))
mtext("F", font=2, side=3, line=1, adj=-0.15, cex = 1)
abline(h=seq(0,15,1), col="gray", lty="dotted")
abline(v=0, col="red", lwd=3, lty=2)

######
par(mfrow = c(1, 1)) #setting back to one per page

############## Mean calculations
bear_delta$delta_UC2 <- as.numeric(bear_delta$delta_UC2)
deer_delta$delta_UC2 <- as.numeric(deer_delta$delta_UC2)
mg_delta$delta_UC2 <- as.numeric(mg_delta$delta_UC2)

bear_delta <- na.omit(bear_delta$delta_UC2)
mean(bear_delta)

deer_delta <- na.omit(deer_delta$delta_UC2)
mean(deer_delta)
median(deer_delta)

mg_delta <- na.omit(mg_delta$delta_UC2)
mean(mg_delta)
median(mg_delta)
