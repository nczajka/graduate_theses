#!/bin/bash
#SBATCH --account=rrg-shaferab
#SBATCH --job-name=bwa-bam-sort
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000
#SBATCH --time=0-11:00 # time (DD-HH:MM)

module load StdEnv/2016.4
module load bwa
module load samtools
echo ${1}

bwa mem -M -t 16 -R "@RG\\tID:${1}\\tSM:${1}\\tLB:${1}\\tPL:ILLUMINA" \
${2} ${4}/${1}_trim_R1_001.fastq.gz ${4}/${1}_trim_R2_001.fastq.gz | \
samtools sort -@ 16 -o ${4}/${1}.sorted_reads.bam &&
samtools flagstat ${4}/${1}.sorted_reads.bam > ${4}/${1}.sorted_reads.flagstat
#END
